package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class ButtonTest {
    val ns: String? = null

    @Test
    fun completeButtonXMLShouldBeParsed() {
        val xml = "<button id=\"448bc4fc-8ebd-4ecb-ae5c-c2e0df532039\" " +
                "lockable=\"false\" lockable-after=\"3.0\">" +
                "Test" +
                "</button>"

        val parser = XmlHelper.buildParser(xml, ns)

        val buttonParsed = Button.readElement(parser, ns)

        Assert.assertEquals(
            Button(
                id = "448bc4fc-8ebd-4ecb-ae5c-c2e0df532039",
                text = "Test",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(3.0)
            ),
            buttonParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun buttonWithoutIdAttributeShouldThrowAnException() {
        val xml = "<button lockable=\"false\" lockable-after=\"3.0\">" +
                "Test" +
                "</button>"

        val parser = XmlHelper.buildParser(xml, ns)

        Button.readElement(parser, ns)

        Assert.assertTrue(false)
    }

    @Test
    fun buttonWithoutTextShouldBeParsed() {
        val xml = "<button id=\"3d13200b-75f2-498b-8164-0817e2e9484d\" " +
                "lockable=\"false\" lockable-after=\"3.0\">" +
                "</button>"

        val parser = XmlHelper.buildParser(xml, ns)

        val buttonParsed = Button.readElement(parser, ns)

        Assert.assertEquals(
            Button(
                id = "3d13200b-75f2-498b-8164-0817e2e9484d",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(3.0)
            ),
            buttonParsed
        )
    }

    @Test
    fun buttonWithoutLockableAttributeShouldBeParsed() {
        val xml = "<button id=\"af6325d5-18c9-4e83-ad9c-4a16218a9f29\" " +
                " lockable-after=\"3.0\">" +
                "Test" +
                "</button>"

        val parser = XmlHelper.buildParser(xml, ns)

        val buttonParsed = Button.readElement(parser, ns)

        Assert.assertEquals(
            Button(
                id = "af6325d5-18c9-4e83-ad9c-4a16218a9f29",
                text = "Test",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(3.0)
            ),
            buttonParsed
        )
    }

    @Test
    fun buttonWithoutLockableAfterAttributeShouldBeParsed() {
        val xml = "<button id=\"592d84bb-c4ed-49dc-aa42-5782affe0e2e\" " +
                "lockable=\"false\">" +
                "</button>"

        val parser = XmlHelper.buildParser(xml, ns)

        val buttonParsed = Button.readElement(parser, ns)

        Assert.assertEquals(
            Button(
                id = "592d84bb-c4ed-49dc-aa42-5782affe0e2e",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(3.0)
            ),
            buttonParsed
        )
    }

    @Test
    fun everyLockableAfterPossibleValueShouldBeParsed() {
        val xmls = arrayOf(
            "<button id=\"735d3ccd-a129-43e8-846b-fb19d0ec38a5\" " +
                    "lockable=\"false\" lockable-after=\"3.0\">" +
                    "</button>",
            "<button id=\"9b13446e-7cd5-4f79-a1de-e03ca79c4207\" " +
                    "lockable=\"false\" lockable-after=\"1\">" +
                    "</button>",
            "<button id=\"40d9899b-a115-4584-b92f-e05bdf3c864f\" " +
                    "lockable=\"false\" lockable-after=\"2.5\">" +
                    "</button>",
            "<button id=\"76bc6e8e-355c-441d-9208-58b94fefa97c\" " +
                    "lockable=\"false\" lockable-after=\"onClick\">" +
                    "</button>",
            "<button id=\"30586d55-33ab-4532-9e64-db1f0b50bbb4\" " +
                    "lockable=\"false\" lockable-after=\"onDoubleClick\">" +
                    "</button>",
        )

        val expected = arrayOf(
            Button(
                id = "735d3ccd-a129-43e8-846b-fb19d0ec38a5",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(3.0)
            ),
            Button(
                id = "9b13446e-7cd5-4f79-a1de-e03ca79c4207",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(1.0)
            ),
            Button(
                id = "40d9899b-a115-4584-b92f-e05bdf3c864f",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(2.5)
            ),
            Button(
                id = "76bc6e8e-355c-441d-9208-58b94fefa97c",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.ON_CLICK
            ),
            Button(
                id = "30586d55-33ab-4532-9e64-db1f0b50bbb4",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.ON_DOUBLE_CLICK
            )
        )

        for (i in xmls.indices) {
            val parser = XmlHelper.buildParser(xmls[i], ns)

            val buttonParsed = Button.readElement(parser, ns)

            Assert.assertEquals(expected[i], buttonParsed)
        }
    }

    @Test
    fun everyLockablePossibleValueShouldBeParsed() {
        val xmls = arrayOf(
            "<button id=\"23447930-aea2-4157-9d7d-2400fad5b60d\" " +
                    "lockable=\"false\" lockable-after=\"3.0\">" +
                    "</button>",
            "<button id=\"4ea553cf-e08e-4e6a-9d84-8131ffa16c10\" " +
                    "lockable=\"true\" lockable-after=\"3.0\">" +
                    "</button>"
        )

        val expected = arrayOf(
            Button(
                id = "23447930-aea2-4157-9d7d-2400fad5b60d",
                text = "",
                lockable = false,
                lockableAfter = Button.LockableAfter.SECONDS(3.0)
            ),
            Button(
                id = "4ea553cf-e08e-4e6a-9d84-8131ffa16c10",
                text = "",
                lockable = true,
                lockableAfter = Button.LockableAfter.SECONDS(3.0)
            )
        )

        for (i in xmls.indices) {
            val parser = XmlHelper.buildParser(xmls[i], ns)

            val buttonParsed = Button.readElement(parser, ns)

            Assert.assertEquals(expected[i], buttonParsed)
        }
    }
}