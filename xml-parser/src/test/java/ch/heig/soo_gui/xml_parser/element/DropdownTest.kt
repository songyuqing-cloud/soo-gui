package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import junit.framework.Assert.assertTrue
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class DropdownTest {
    val ns: String? = null

    @Test
    fun completeDropdownShouldBeParsed() {
        val xml = "<dropdown id=\"0c2a0b70-79c9-4912-aded-11c3153c6445\">" +
                "<option>Test 1</option>" +
                "<option>Test 2</option>" +
                "</dropdown>"

        val parser = XmlHelper.buildParser(xml, ns)

        val dropdownParsed = Dropdown.readElement(parser, ns)

        Assert.assertEquals(
            Dropdown(
                id = "0c2a0b70-79c9-4912-aded-11c3153c6445",
                options = listOf(
                    Option(content = "Test 1", value = "Test 1"),
                    Option(content = "Test 2", value = "Test 2"),
                ),
                selectedOption = null
            ),
            dropdownParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun dropdownWithoutIdAttributeShouldThrowAnException() {
        val xml = "<dropdown>" +
                "<option>Test 1</option>" +
                "<option>Test 2</option>" +
                "</dropdown>"

        val parser = XmlHelper.buildParser(xml, ns)

        Dropdown.readElement(parser, ns)

        assertTrue(false)
    }

    @Test
    fun dropdownWithoutContentShouldBeParsed() {
        val xml = "<dropdown id=\"999308d7-5aec-4c0e-a237-f12cb9981202\">" +
                "</dropdown>"

        val parser = XmlHelper.buildParser(xml, ns)

        val dropdownParsed = Dropdown.readElement(parser, ns)

        Assert.assertEquals(
            Dropdown(
                id = "999308d7-5aec-4c0e-a237-f12cb9981202",
                options = emptyList(),
                selectedOption = null
            ),
            dropdownParsed
        )
    }

    @Test
    fun dropdownWithDefaultOptionShouldHaveItSelected() {
        val xml = "<dropdown id=\"48cd5e61-f3b3-45b1-9eb0-548a40209147\">" +
                "<option default=\"true\">Test 1</option>" +
                "<option>Test 2</option>" +
                "</dropdown>"

        val parser = XmlHelper.buildParser(xml, ns)

        val dropdownParsed = Dropdown.readElement(parser, ns)

        Assert.assertEquals(
            Dropdown(
                id = "48cd5e61-f3b3-45b1-9eb0-548a40209147",
                options = listOf(
                    Option(content = "Test 1", value = "Test 1", default = true),
                    Option(content = "Test 2", value = "Test 2"),
                ),
                selectedOption = Option(content = "Test 1", value = "Test 1", default = true)
            ),
            dropdownParsed
        )
    }
}