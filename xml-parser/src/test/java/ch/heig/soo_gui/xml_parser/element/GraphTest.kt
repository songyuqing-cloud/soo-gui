package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class GraphTest {
    val ns: String? = null

    @Test
    fun completeGraphShouldBeParsed() {
        val xml = "<graph id=\"6a476333-6092-4776-98d6-0849c5f68857\"" +
                " type=\"table\">" +
                "<series id=\"91cc80af-e9cc-4b63-bcc6-27ab83db7ee5\" />" +
                "<series id=\"a904b5e5-78e9-4869-b9c6-c881a0d15268\" />" +
                "</graph>"

        val parser = XmlHelper.buildParser(xml, ns)

        val graphParsed = Graph.readElement(parser, ns)

        Assert.assertEquals(
            Graph(
                id = "6a476333-6092-4776-98d6-0849c5f68857",
                type = Graph.Type.TABLE,
                series = listOf(
                    Series(id="91cc80af-e9cc-4b63-bcc6-27ab83db7ee5", name=""),
                    Series(id="a904b5e5-78e9-4869-b9c6-c881a0d15268", name=""),
                )
            ),
            graphParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun graphWithoutIdAttributeShouldThrowAnException() {
        val xml = "<graph type=\"table\">" +
                "<series id=\"ee21fd12-535d-405f-acf9-21fbe24d8b65\" />" +
                "<series id=\"c58111e4-c8fb-4ef0-959f-f1bd689ae577\" />" +
                "</graph>"

        val parser = XmlHelper.buildParser(xml, ns)

        Graph.readElement(parser, ns)

        Assert.assertTrue(false)
    }

    @Test
    fun graphWithoutTypeAttributeShouldBeParsed() {
        val xml = "<graph id=\"3e6658fa-697d-42a1-b576-ef67b5d72730\">" +
                "<series id=\"a8802b34-4e1d-488b-9e64-209e3cb1c768\" />" +
                "<series id=\"05c65ed5-612f-4444-92d4-41281147b467\" />" +
                "</graph>"

        val parser = XmlHelper.buildParser(xml, ns)

        val graphParsed = Graph.readElement(parser, ns)

        Assert.assertEquals(
            Graph(
                id = "3e6658fa-697d-42a1-b576-ef67b5d72730",
                type = Graph.Type.TABLE,
                series = listOf(
                    Series(id="a8802b34-4e1d-488b-9e64-209e3cb1c768", name=""),
                    Series(id="05c65ed5-612f-4444-92d4-41281147b467", name=""),
                )
            ),
            graphParsed
        )
    }

    @Test
    fun graphWithoutContentShouldBeParsed() {
        val xml = "<graph id=\"02dc4304-5e6b-4910-9bfc-0ab57e40fd24\" type=\"table\">" +
                "</graph>"

        val parser = XmlHelper.buildParser(xml, ns)

        val graphParsed = Graph.readElement(parser, ns)

        Assert.assertEquals(
            Graph(
                id = "02dc4304-5e6b-4910-9bfc-0ab57e40fd24",
                type = Graph.Type.TABLE,
                series = emptyList()
            ),
            graphParsed
        )
    }

    @Test
    fun everyTypePossibleValueShouldBeParsed() {
        val xmls = arrayOf(
            "<graph id=\"25f536d7-b6e1-446a-84f4-019bbd56b3ba\" type=\"table\">" +
                    "</graph>",
            "<graph id=\"3e58498e-8244-4b13-8ab7-d836e1c5b870\" type=\"line\">" +
                    "</graph>",
        )

        val expected = arrayOf(
            Graph(
                id = "25f536d7-b6e1-446a-84f4-019bbd56b3ba",
                type = Graph.Type.TABLE,
                series = emptyList()
            ),
            Graph(
                id = "3e58498e-8244-4b13-8ab7-d836e1c5b870",
                type = Graph.Type.LINE,
                series = emptyList()
            ),
        )

        for (i in xmls.indices) {
            val parser = XmlHelper.buildParser(xmls[i], ns)

            val graphParsed = Graph.readElement(parser, ns)

            Assert.assertEquals(expected[i], graphParsed)
        }
    }
}