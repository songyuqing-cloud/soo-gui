package ch.heig.soo_gui.xml_parser

import ch.heig.soo_gui.xml_parser.element.Text
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

/**
 * Unit tests for the messages received.
 *
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class MessageTest {
    @Test
    fun singleMessageShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<messages>" +
                "<message to=\"ec9aad6f-2cbd-4559-88fd-113a8d30e562\"><text>Inside text</text></message>" +
                "</messages>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MESSAGES, result.type)
        Assert.assertEquals(
            listOf(
                Message(
                    "ec9aad6f-2cbd-4559-88fd-113a8d30e562",
                    listOf(Text(content = "Inside text"))
                ),
            ),
            result.content,
        )
    }

    @Test
    fun messageWithEmptyContentShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<messages>" +
                "<message to=\"f2e5f1c2-4502-4f72-86c7-93aaf74f202d\"></message>" +
                "</messages>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MESSAGES, result.type)
        Assert.assertEquals(
            listOf(Message(to = "f2e5f1c2-4502-4f72-86c7-93aaf74f202d")),
            result.content
        )
    }

    @Test
    fun messageWithOnlyAStringShouldConvertToTextElement() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<messages>" +
                "<message to=\"4c6724d5-e381-45fb-87bb-83d5a4eff4e7\">A string test</message>" +
                "</messages>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MESSAGES, result.type)
        Assert.assertEquals(
            listOf(
                Message(
                    "4c6724d5-e381-45fb-87bb-83d5a4eff4e7",
                    listOf(Text(content = "A string test"))
                ),
            ),
            result.content,
        )

    }

    @Test
    fun singleMessageWithManyElementToParseShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<messages>" +
                "<message to=\"3c0c1023-3a63-442b-b3f3-9cf788849b25\"><text>Inside text 1</text><text>Inside text 2</text></message>" +
                "</messages>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MESSAGES, result.type)
        Assert.assertEquals(
            listOf(
                Message(
                    "3c0c1023-3a63-442b-b3f3-9cf788849b25",
                    listOf(Text(content = "Inside text 1"), Text(content = "Inside text 2"))
                )
            ),
            result.content,
        )
    }

    @Test
    fun manyMessagesToParseShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<messages>" +
                "<message to=\"dd24a016-10ee-431d-aef4-e136cd49e29c\">A string test</message>" +
                "<message to=\"2741497c-5874-40cd-9aef-8ff53213b639\"><text>Inside text</text></message>" +
                "<message to=\"977cd93b-fae5-481e-a451-9b75c11182a0\"><text>Inside text 1</text><text>Inside text 2</text></message>" +
                "</messages>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MESSAGES, result.type)
        Assert.assertEquals(
            listOf(
                Message(
                    "dd24a016-10ee-431d-aef4-e136cd49e29c",
                    listOf(Text(content = "A string test"))
                ),
                Message(
                    "2741497c-5874-40cd-9aef-8ff53213b639",
                    listOf(Text(content = "Inside text"))
                ),
                Message(
                    "977cd93b-fae5-481e-a451-9b75c11182a0",
                    listOf(Text(content = "Inside text 1"), Text(content = "Inside text 2"))
                )
            ),
            result.content,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun messageWithoutToAttributeShouldThrowAnException() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<messages>" +
                "<message><text>Will fail</text></message>" +
                "</messages>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        parser.parse(input)

        Assert.assertTrue(false)
    }

    @Test(expected = IllegalArgumentException::class)
    fun messageWithBlankToAttributeShouldThrowAnException() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<messages>" +
                "<message to=\"\"><text>Will fail</text></message>" +
                "</messages>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        parser.parse(input)

        Assert.assertTrue(false)
    }
}