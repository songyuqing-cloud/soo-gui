package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class LabelTest {
    val ns: String? = null

    @Test
    fun completeLabelShouldBeParsed() {
        val xml = "<label for=\"0dddecbd-06bc-42dd-b4e0-45b816d21a79\">Test</label>"

        val parser = XmlHelper.buildParser(xml, ns)

        val labelParsed = Label.readElement(parser, ns)

        Assert.assertEquals(
            Label(
                forId = "0dddecbd-06bc-42dd-b4e0-45b816d21a79",
                content = "Test"
            ),
            labelParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun labelWithoutForAttributeShouldThrowAnException() {
        val xml = "<label>Test</label>"

        val parser = XmlHelper.buildParser(xml, ns)

        Label.readElement(parser, ns)

        Assert.assertTrue(false)
    }

    @Test
    fun labelWithoutContentShouldBeParsed() {
        val xml = "<label for=\"e028f80d-3b0f-410e-873f-c77a712bef0c\"></label>"

        val parser = XmlHelper.buildParser(xml, ns)

        val labelParsed = Label.readElement(parser, ns)

        Assert.assertEquals(
            Label(
                forId = "e028f80d-3b0f-410e-873f-c77a712bef0c",
                content = ""
            ),
            labelParsed
        )
    }
}