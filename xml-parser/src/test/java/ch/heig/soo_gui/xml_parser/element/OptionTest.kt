package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class OptionTest {
    val ns: String? = null

    @Test
    fun completeOptionShouldBeParsed() {
        val xml = "<option value=\"testValue\" default=\"false\">Test</option>"

        val parser = XmlHelper.buildParser(xml, ns)

        val optionParsed = Option.readElement(parser, ns)

        Assert.assertEquals(
            Option(
                content = "Test",
                value = "testValue",
                default = false
            ),
            optionParsed
        )
    }

    @Test
    fun optionWithoutValueAttributeShouldUseContentInstead() {
        val xml = "<option default=\"false\">Test</option>"

        val parser = XmlHelper.buildParser(xml, ns)

        val optionParsed = Option.readElement(parser, ns)

        Assert.assertEquals(
            Option(
                content = "Test",
                value = "Test",
                default = false
            ),
            optionParsed
        )
    }

    @Test
    fun optionWithoutDefaultAttributeShouldBeParsed() {
        val xml = "<option value=\"testValue\">Test</option>"

        val parser = XmlHelper.buildParser(xml, ns)

        val optionParsed = Option.readElement(parser, ns)

        Assert.assertEquals(
            Option(
                content = "Test",
                value = "testValue",
                default = false
            ),
            optionParsed
        )
    }

    @Test
    fun emptyOptionShouldBeParsed() {
        val xml = "<option/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val optionParsed = Option.readElement(parser, ns)

        Assert.assertEquals(
            Option(
                content = "",
                value = "",
                default = false
            ),
            optionParsed
        )
    }
}