package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class PointTest {
    val ns: String? = null

    @Test
    fun completePointShouldBeParsed() {
        val xml = "<point " +
                "x=\"xValue\" " +
                "y=\"yValue\" " +
                "z=\"zValue\"/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val pointParsed = Point.readElement(parser, ns)

        Assert.assertEquals(
            Point(
                x = "xValue",
                y = "yValue",
                z = "zValue",
            ),
            pointParsed
        )
    }

    @Test
    fun pointWithoutXAttributeShouldBeParsed() {
        val xml = "<point " +
                "y=\"yValue\" " +
                "z=\"zValue\" " +
                "/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val pointParsed = Point.readElement(parser, ns)

        Assert.assertEquals(
            Point(
                x = "",
                y = "yValue",
                z = "zValue",
            ),
            pointParsed
        )
    }

    @Test
    fun pointWithoutYAttributeShouldBeParsed() {
        val xml = "<point " +
                "x=\"xValue\" " +
                "z=\"zValue\" " +
                "/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val pointParsed = Point.readElement(parser, ns)

        Assert.assertEquals(
            Point(
                x = "xValue",
                y = "",
                z = "zValue",
            ),
            pointParsed
        )
    }

    @Test
    fun pointWithoutZAttributeShouldBeParsed() {
        val xml = "<point " +
                "x=\"xValue\" " +
                "y=\"yValue\" " +
                "/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val pointParsed = Point.readElement(parser, ns)

        Assert.assertEquals(
            Point(
                x = "xValue",
                y = "yValue",
                z = "",
            ),
            pointParsed
        )
    }

}