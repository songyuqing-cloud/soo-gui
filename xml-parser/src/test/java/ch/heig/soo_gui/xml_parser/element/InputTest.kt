package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class InputTest {
    val ns: String? = null

    @Test
    fun completeInputShouldBeParsed() {
        val xml = "<input id=\"fc9fd166-cbdf-4bf0-96c3-5973876fad05\"" +
                " value=\"testValue\">" +
                "Test" +
                "</input>"

        val parser = XmlHelper.buildParser(xml, ns)

        val inputParsed = Input.readElement(parser, ns)

        Assert.assertEquals(
            Input(
                id = "fc9fd166-cbdf-4bf0-96c3-5973876fad05",
                value = "testValue"
            ),
            inputParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun inputWithoutIdAttributeShouldThrowAnException() {
        val xml = "<input>Test</input>"

        val parser = XmlHelper.buildParser(xml, ns)

        Input.readElement(parser, ns)

        Assert.assertTrue(false)
    }

    @Test
    fun inputWithoutValueAttributeShouldUseContentInstead() {
        val xml = "<input id=\"bc79f536-40e9-4ac2-8b16-e66eb5fd76ef\">" +
                "Test" +
                "</input>"

        val parser = XmlHelper.buildParser(xml, ns)

        val inputParsed = Input.readElement(parser, ns)

        Assert.assertEquals(
            Input(
                id = "bc79f536-40e9-4ac2-8b16-e66eb5fd76ef",
                value = "Test"
            ),
            inputParsed
        )
    }

    @Test
    fun inputWithoutValueAttributeOrContentShouldShouldBeParsed() {
        val xml = "<input id=\"c1f05620-9a6d-407d-bcda-27658163e7ea\"/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val inputParsed = Input.readElement(parser, ns)

        Assert.assertEquals(
            Input(
                id = "c1f05620-9a6d-407d-bcda-27658163e7ea",
                value = ""
            ),
            inputParsed
        )
    }

    @Test
    fun inputWithoutContentShouldBeParsed() {
        val xml = "<input id=\"dda9709c-7127-4351-9ec6-4fe94bceb104\"" +
                " value=\"testValue\"/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val inputParsed = Input.readElement(parser, ns)

        Assert.assertEquals(
            Input(
                id = "dda9709c-7127-4351-9ec6-4fe94bceb104",
                value = "testValue"
            ),
            inputParsed
        )
    }
}