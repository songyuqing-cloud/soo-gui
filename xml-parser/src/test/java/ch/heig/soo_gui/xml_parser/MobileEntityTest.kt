package ch.heig.soo_gui.xml_parser

import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

/**
 * Unit test of the mobile entity received.
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class MobileEntityTest {
    @Test
    fun completeMobileEntityShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"10f1e79f-486b-4103-bd01-f9b8864479cf\">" +
                "<name>Test</name>" +
                "<description>Short description</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(
            listOf(
                MobileEntity(
                    spid = "10f1e79f-486b-4103-bd01-f9b8864479cf",
                    name = "Test",
                    description = "Short description"
                )
            ), result.content
        )
    }

    @Test
    fun emptyMobileEntitiesShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(emptyList<MobileEntity>(), result.content)
    }

    @Test
    fun manyMobileEntitiesShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"4278c640-3fdc-40af-8d74-7b9a0514b556\">" +
                "<name>Test 1</name>" +
                "<description>Short description 1</description>" +
                "</mobile-entity>" +
                "<mobile-entity spid=\"212b7b9e-e7f1-4291-b557-649a3dd6efa4\">" +
                "<name>Test 2</name>" +
                "<description>Short description 2</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(
            listOf(
                MobileEntity(
                    spid = "4278c640-3fdc-40af-8d74-7b9a0514b556",
                    name = "Test 1",
                    description = "Short description 1"
                ),
                MobileEntity(
                    spid = "212b7b9e-e7f1-4291-b557-649a3dd6efa4",
                    name = "Test 2",
                    description = "Short description 2"
                ),
            ), result.content
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun mobileEntityWithoutSpidAttributeShouldThrowAnException() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity>" +
                "<name>Test</name>" +
                "<description>Short description</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        parser.parse(input)

        assertTrue(false)
    }

    @Test(expected = IllegalArgumentException::class)
    fun mobileEntityWithoutNameShouldThrowAnException() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"078c15b1-f3b8-4d59-bbe4-4e75dd2aef4c\">" +
                "<description>Short description</description>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        parser.parse(input)

        assertTrue(false)
    }

    @Test
    fun mobileEntityWithoutDescriptionShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<mobile-entities>" +
                "<mobile-entity spid=\"26bfd4be-c0e8-4355-974e-3abe3cdd2057\">" +
                "<name>Test</name>" +
                "</mobile-entity>" +
                "</mobile-entities>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        assertEquals(XmlParser.ContentType.MOBILE_ENTITIES, result.type)
        assertEquals(
            listOf(
                MobileEntity(
                    spid = "26bfd4be-c0e8-4355-974e-3abe3cdd2057",
                    name = "Test",
                    description = null
                )
            ), result.content
        )
    }
}


