package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class ColTest {
    val ns: String? = null

    @Test
    fun completeColShouldBeParsed() {
        val xml = "<col id=\"f7e42530-fff8-4bff-9b09-cb424ad35e6b\" " +
                "span=\"auto\" offset=\"0\">" +
                "<text>Test</text>" +
                "</col>"

        val parser = XmlHelper.buildParser(xml, ns)

        val colParsed = Col.readElement(parser, ns)

        Assert.assertEquals(
            Col(
                id = "f7e42530-fff8-4bff-9b09-cb424ad35e6b",
                span = Col.Spanning.AUTO,
                offset = 0,
                listOf(Text(content = "Test"))
            ),
            colParsed
        )
    }

    @Test
    fun colWithoutIdAttributeShouldBeParsed() {
        val xml = "<col span=\"auto\" offset=\"0\">" +
                "<text>Test</text>" +
                "</col>"

        val parser = XmlHelper.buildParser(xml, ns)

        val colParsed = Col.readElement(parser, ns)

        Assert.assertEquals(
            Col(
                id = null,
                span = Col.Spanning.AUTO,
                offset = 0,
                listOf(Text(content = "Test"))
            ),
            colParsed
        )
    }

    @Test
    fun colWithoutSpanAttributeShouldBeParsed() {
        val xml = "<col id=\"53b530e2-1667-4549-aab9-c446fa56acc4\" " +
                " offset=\"0\">" +
                "<text>Test</text>" +
                "</col>"

        val parser = XmlHelper.buildParser(xml, ns)

        val colParsed = Col.readElement(parser, ns)

        Assert.assertEquals(
            Col(
                id = "53b530e2-1667-4549-aab9-c446fa56acc4",
                span = Col.Spanning.AUTO,
                offset = 0,
                listOf(Text(content = "Test"))
            ),
            colParsed
        )
    }

    @Test
    fun colWithoutOffsetAttributeShouldBeParsed() {
        val xml = "<col id=\"6494b635-8534-4c47-a937-20cc5483a1c8\" " +
                "span=\"auto\">" +
                "<text>Test</text>" +
                "</col>"

        val parser = XmlHelper.buildParser(xml, ns)

        val colParsed = Col.readElement(parser, ns)

        Assert.assertEquals(
            Col(
                id = "6494b635-8534-4c47-a937-20cc5483a1c8",
                span = Col.Spanning.AUTO,
                offset = 0,
                listOf(Text(content = "Test"))
            ),
            colParsed
        )
    }

    @Test
    fun colWithoutContentShouldBeParsed() {
        val xml = "<col id=\"82b79671-e7a5-4aee-b234-b871c801486b\" " +
                "span=\"auto\" offset=\"0\">" +
                "</col>"

        val parser = XmlHelper.buildParser(xml, ns)

        val colParsed = Col.readElement(parser, ns)

        Assert.assertEquals(
            Col(
                id = "82b79671-e7a5-4aee-b234-b871c801486b",
                span = Col.Spanning.AUTO,
                offset = 0,
                emptyList()
            ),
            colParsed
        )
    }

    @Test
    fun everySpanPossibleValueShouldBeParsed() {
        val xmls = arrayOf(
            "<col id=\"51191f63-caa1-456f-bcd5-eadb4a400b75\" " +
                    "span=\"auto\" offset=\"0\">" +
                    "<text>Test</text>" +
                    "</col>",
            "<col id=\"e7944d4c-7a2d-4e22-85d0-ccb915e99ec9\" " +
                    "span=\"1\" offset=\"0\">" +
                    "<text>Test</text>" +
                    "</col>",
            "<col id=\"e21fc386-13b3-40a0-9c49-0185eb157a42\" " +
                    "span=\"2\" offset=\"0\">" +
                    "<text>Test</text>" +
                    "</col>"
        )

        val expected = arrayOf(
            Col(
                id = "51191f63-caa1-456f-bcd5-eadb4a400b75",
                span = Col.Spanning.AUTO,
                offset = 0,
                listOf(Text(content = "Test"))
            ),
            Col(
                id = "e7944d4c-7a2d-4e22-85d0-ccb915e99ec9",
                span = Col.Spanning.FIXED(1),
                offset = 0,
                listOf(Text(content = "Test"))
            ),
            Col(
                id = "e21fc386-13b3-40a0-9c49-0185eb157a42",
                span = Col.Spanning.FIXED(2),
                offset = 0,
                listOf(Text(content = "Test"))
            )
        )

        for (i in xmls.indices) {
            val parser = XmlHelper.buildParser(xmls[i], ns)

            val colParsed = Col.readElement(parser, ns)

            Assert.assertEquals(expected[i], colParsed)
        }
    }
}