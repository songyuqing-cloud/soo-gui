package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class LayoutTest {
    val ns: String? = null

    @Test
    fun completeLayoutShouldBeParsed() {
        val xml = "<layout id=\"c4c33f27-dbb9-4cac-a474-a5fdde52a209\" " +
                "max-cols=\"12\">" +
                "<row id=\"72dddbea-bdd2-4251-befe-3c7415d4422a\"></row>" +
                "<row id=\"e88b2eac-02e6-430d-b46c-e9dc367f5257\"></row>" +
                "</layout>"

        val parser = XmlHelper.buildParser(xml, ns)

        val layoutParsed = Layout.readElement(parser, ns)

        Assert.assertEquals(
            Layout(
                id = "c4c33f27-dbb9-4cac-a474-a5fdde52a209",
                maxCols= 12,
                listOf(
                    Row(id = "72dddbea-bdd2-4251-befe-3c7415d4422a"),
                    Row(id = "e88b2eac-02e6-430d-b46c-e9dc367f5257"),
                )
            ),
            layoutParsed
        )
    }

    @Test
    fun layoutWithoutIdAttributeShouldBeParsed() {
        val xml = "<layout max-cols=\"12\">" +
                "<row id=\"16f48a5e-d548-4bdb-ba9a-61870a18934b\"></row>" +
                "<row id=\"f5a2067f-f8f0-4539-86e7-c292833bd55c\"></row>" +
                "</layout>"

        val parser = XmlHelper.buildParser(xml, ns)

        val layoutParsed = Layout.readElement(parser, ns)

        Assert.assertEquals(
            Layout(
                id = null,
                maxCols= 12,
                listOf(
                    Row(id = "16f48a5e-d548-4bdb-ba9a-61870a18934b"),
                    Row(id = "f5a2067f-f8f0-4539-86e7-c292833bd55c"),
                )
            ),
            layoutParsed
        )
    }

    @Test
    fun layoutWithoutMaxColsAttributeShouldBeParsed() {
        val xml = "<layout id=\"23be5dd5-46e3-4421-8176-62484b74ad87\">" +
                "<row id=\"ee289176-371f-4df2-9ad3-9280f6de6961\"></row>" +
                "<row id=\"209c2a86-e48d-4df1-89d7-e4623f696137\"></row>" +
                "</layout>"

        val parser = XmlHelper.buildParser(xml, ns)

        val layoutParsed = Layout.readElement(parser, ns)

        Assert.assertEquals(
            Layout(
                id = "23be5dd5-46e3-4421-8176-62484b74ad87",
                maxCols= 12,
                listOf(
                    Row(id = "ee289176-371f-4df2-9ad3-9280f6de6961"),
                    Row(id = "209c2a86-e48d-4df1-89d7-e4623f696137"),
                )
            ),
            layoutParsed
        )
    }

    @Test
    fun layoutWithoutContentShouldBeParsed() {
        val xml = "<layout id=\"434ec69f-d6fe-4c5c-896a-d0e1ecd277f5\" " +
                " max-cols=\"12\">" +
                "</layout>"

        val parser = XmlHelper.buildParser(xml, ns)

        val layoutParsed = Layout.readElement(parser, ns)

        Assert.assertEquals(
            Layout(
                id = "434ec69f-d6fe-4c5c-896a-d0e1ecd277f5",
                maxCols= 12,
                emptyList()
            ),
            layoutParsed
        )
    }
}