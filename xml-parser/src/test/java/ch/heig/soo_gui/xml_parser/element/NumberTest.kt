package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class NumberTest {
    val ns: String? = null

    @Test
    fun completeNumberShouldBeParsed() {
        val xml = "<number id=\"be09bd53-ddfd-48a3-b5ff-1beff10aa9f6\"" +
                " step=\"1\" value=\"2.5\">" +
                "3.5" +
                "</number>"

        val parser = XmlHelper.buildParser(xml, ns)

        val numberParsed = Number.readElement(parser, ns)

        Assert.assertEquals(
            Number(
                id = "be09bd53-ddfd-48a3-b5ff-1beff10aa9f6",
                value = 2.5,
                step = 1.0
            ),
            numberParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun numberWithoutIdAttributeShouldThrowAnException() {
        val xml = "<number step=\"1\" value=\"2.5\">3.5</number>"

        val parser = XmlHelper.buildParser(xml, ns)

        Number.readElement(parser, ns)

        Assert.assertTrue(false)
    }

    @Test
    fun numberWithoutValueAttributeShouldUseContentInstead() {
        val xml = "<number id=\"56871be9-30e2-4898-bfaa-ab0b075c24e4\"" +
                " step=\"1\">" +
                "3.5" +
                "</number>"

        val parser = XmlHelper.buildParser(xml, ns)

        val numberParsed = Number.readElement(parser, ns)

        Assert.assertEquals(
            Number(
                id = "56871be9-30e2-4898-bfaa-ab0b075c24e4",
                step = 1.0,
                value = 3.5
            ),
            numberParsed
        )
    }

    @Test
    fun numberWithoutValueAttributeOrContentShouldShouldBeParsed() {
        val xml = "<number id=\"a08e3c1a-f935-4232-a55d-3f718ccde3aa\"/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val numberParsed = Number.readElement(parser, ns)

        Assert.assertEquals(
            Number(
                id = "a08e3c1a-f935-4232-a55d-3f718ccde3aa",
                step = 1.0,
                value = 0.0
            ),
            numberParsed
        )
    }

    @Test
    fun numberWithoutStepAttributeShouldShouldBeParsed() {
        val xml = "<number id=\"05bc4ce9-4fc7-45b2-b187-28e163a597c7\"" +
                " value=\"2.5\">3.5</number>"

        val parser = XmlHelper.buildParser(xml, ns)

        val numberParsed = Number.readElement(parser, ns)

        Assert.assertEquals(
            Number(
                id = "05bc4ce9-4fc7-45b2-b187-28e163a597c7",
                step = 1.0,
                value = 2.5
            ),
            numberParsed
        )
    }

    @Test
    fun numberWithoutContentShouldBeParsed() {
        val xml = "<number id=\"083d89c1-b886-4fef-a197-144db5b60b1b\"" +
                " step=\"1\" value=\"2.5\"/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val numberParsed = Number.readElement(parser, ns)

        Assert.assertEquals(
            Number(
                id = "083d89c1-b886-4fef-a197-144db5b60b1b",
                step = 1.0,
                value = 2.5
            ),
            numberParsed
        )
    }
}