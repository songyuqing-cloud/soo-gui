package ch.heig.soo_gui.xml_parser

import org.junit.Assert
import org.junit.Test

class EventsTest {
    @Test
    fun completeEventShouldHaveTheCorrectOutput() {
        val expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<events>" +
                "<event from=\"564c4a49-8a60-4f36-b468-6e66f7c73e55\" action=\"clickDown\">" +
                "</event>" +
                "</events>"

        val events = listOf(
            Event(
                from = "564c4a49-8a60-4f36-b468-6e66f7c73e55",
                action = Event.Action.CLICK_DOWN
            )
        )

        val result = XmlWriter.writeEvents(events)

        Assert.assertEquals(expected, result)
    }

    @Test
    fun emptyEventsShouldHaveTheCorrectOutput() {
        val expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<events>" +
                "</events>"

        val events = emptyList<Event>()

        val result = XmlWriter.writeEvents(events)

        Assert.assertEquals(expected, result)
    }

    @Test
    fun manyEventsShouldHaveTheCorrectOutput() {
        val expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<events>" +
                "<event from=\"8b16ae3c-ff11-4628-a4d5-ca831cb3113a\" action=\"clickDown\">" +
                "</event>" +
                "<event from=\"6eb25bbe-10ad-424a-a264-fd25dc49106e\" action=\"clickDown\">" +
                "</event>" +
                "</events>"

        val events = listOf(
            Event(
                from = "8b16ae3c-ff11-4628-a4d5-ca831cb3113a",
                action = Event.Action.CLICK_DOWN
            ),
            Event(
                from = "6eb25bbe-10ad-424a-a264-fd25dc49106e",
                action = Event.Action.CLICK_DOWN
            ),
        )

        val result = XmlWriter.writeEvents(events)

        Assert.assertEquals(expected, result)
    }

    @Test
    fun everyActionAttributeShouldHaveTheCorrectOutput() {
        val expected = listOf(
            "<event from=\"386f7bad-4491-46df-9d74-9b2576a2ec03\" action=\"clickDown\"></event>",
            "<event from=\"80019d8c-431c-40f6-ab14-a88adacd2522\" action=\"clickUp\"></event>",
            "<event from=\"f3e7faad-d163-472f-8ebf-6f1a025baec5\" action=\"valueChanged\">15</event>",
        )

        val events = listOf(
            Event(
                from = "386f7bad-4491-46df-9d74-9b2576a2ec03",
                action = Event.Action.CLICK_DOWN
            ),
            Event(
                from = "80019d8c-431c-40f6-ab14-a88adacd2522",
                action = Event.Action.CLICK_UP
            ),
            Event(
                from = "f3e7faad-d163-472f-8ebf-6f1a025baec5",
                action = Event.Action.VALUE_CHANGED(15.toString())
            ),
        )

        for (i in expected.indices) {
            Assert.assertEquals(expected[i], events[i].toXMLString())
        }
    }
}