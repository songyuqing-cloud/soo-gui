package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class SeriesTest {
    val ns: String? = null

    @Test
    fun completeSeriesShouldBeParsed() {
        val xml = "<series id=\"486ea166-e5e7-4355-b2fe-8f0d44f7d1c1\" name=\"Series 1\">" +
                "<point x=\"1\" />" +
                "<point x=\"2\" />" +
                "</series>"

        val parser = XmlHelper.buildParser(xml, ns)

        val seriesParsed = Series.readElement(parser, ns)

        Assert.assertEquals(
            Series(
                id = "486ea166-e5e7-4355-b2fe-8f0d44f7d1c1",
                name = "Series 1",
                points = listOf(
                    Point(x = "1", y = "", z = ""),
                    Point(x = "2", y = "", z = ""),
                )
            ),
            seriesParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun seriesWithoutIdAttributeShouldThrowAnException() {
        val xml = "<series>" +
                "<point id=\"2e8eb830-3598-4107-b96e-6d7fe07429ba\"></point>" +
                "<point id=\"d4a519cb-031b-4945-aa5b-8102a95b714e\"></point>" +
                "</series>"

        val parser = XmlHelper.buildParser(xml, ns)

        Series.readElement(parser, ns)

        Assert.assertTrue(false)
    }

    @Test
    fun seriesWithoutNameAttributeShouldBeParsed() {
        val xml = "<series id=\"83ec272a-0ad1-4e02-a4e0-d0966ed23ba4\">" +
                "<point x=\"1\" />" +
                "<point x=\"2\" />" +
                "</series>"

        val parser = XmlHelper.buildParser(xml, ns)

        val seriesParsed = Series.readElement(parser, ns)

        Assert.assertEquals(
            Series(
                id = "83ec272a-0ad1-4e02-a4e0-d0966ed23ba4",
                name = "",
                points = listOf(
                    Point(x = "1", y = "", z = ""),
                    Point(x = "2", y = "", z = ""),
                )
            ),
            seriesParsed
        )
    }

    @Test
    fun seriesWithoutContentShouldBeParsed() {
        val xml = "<series id=\"c8b66edf-c053-4e56-9279-c403c2d813b1\"></series>"

        val parser = XmlHelper.buildParser(xml, ns)

        val seriesParsed = Series.readElement(parser, ns)

        Assert.assertEquals(
            Series(
                id = "c8b66edf-c053-4e56-9279-c403c2d813b1",
                name = "",
                points = emptyList()
            ),
            seriesParsed
        )
    }
}