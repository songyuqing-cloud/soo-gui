package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class RowTest {
    val ns: String? = null

    @Test
    fun completeRowShouldBeParsed() {
        val xml = "<row id=\"4f9dea8a-3d8f-41d9-b56b-4b2001581ccf\">" +
                "<col id=\"17e85139-0102-473b-b460-085ef2cd1760\"></col>" +
                "<col id=\"11d65309-a77c-48f2-8d79-80595fc457eb\"></col>" +
                "</row>"

        val parser = XmlHelper.buildParser(xml, ns)

        val rowParsed = Row.readElement(parser, ns)

        Assert.assertEquals(
            Row(
                id = "4f9dea8a-3d8f-41d9-b56b-4b2001581ccf",
                listOf(
                    Col(id = "17e85139-0102-473b-b460-085ef2cd1760"),
                    Col(id = "11d65309-a77c-48f2-8d79-80595fc457eb"),
                )
            ),
            rowParsed
        )
    }

    @Test
    fun rowWithoutIdAttributeShouldBeParsed() {
        val xml = "<row>" +
                "<col id=\"2e8eb830-3598-4107-b96e-6d7fe07429ba\"></col>" +
                "<col id=\"d4a519cb-031b-4945-aa5b-8102a95b714e\"></col>" +
                "</row>"

        val parser = XmlHelper.buildParser(xml, ns)

        val rowParsed = Row.readElement(parser, ns)

        Assert.assertEquals(
            Row(
                id = null,
                listOf(
                    Col(id = "2e8eb830-3598-4107-b96e-6d7fe07429ba"),
                    Col(id = "d4a519cb-031b-4945-aa5b-8102a95b714e"),
                )
            ),
            rowParsed
        )
    }

    @Test
    fun rowWithoutContentShouldBeParsed() {
        val xml = "<row id=\"bcadff4b-d0cd-4996-9f8d-72592fea7179\"></row>"

        val parser = XmlHelper.buildParser(xml, ns)

        val rowParsed = Row.readElement(parser, ns)

        Assert.assertEquals(
            Row(
                id = "bcadff4b-d0cd-4996-9f8d-72592fea7179",
                emptyList()
            ),
            rowParsed
        )
    }
}