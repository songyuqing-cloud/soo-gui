package ch.heig.soo_gui.xml_parser

import ch.heig.soo_gui.xml_parser.element.Layout
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

/**
 * Unit tests for the layout.
 *
 */
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class ModelTest {
    @Test
    fun completeModelShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<model spid=\"04951d80-3ae8-402d-abdd-5083ffcd47e0\">" +
                "<name>Test</name>" +
                "<description>Short description</description>" +
                "<layout></layout>" +
                "</model>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MODEL, result.type)
        Assert.assertEquals(
            Model(
                spid = "04951d80-3ae8-402d-abdd-5083ffcd47e0",
                name = "Test",
                description = "Short description",
                layout = Layout()
            ), result.content
        )
    }

    @Test
    fun modelWithoutSpidShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<model>" +
                "<name>Test</name>" +
                "<description>Short description</description>" +
                "<layout></layout>" +
                "</model>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MODEL, result.type)
        Assert.assertEquals(
            Model(
                spid = null,
                name = "Test",
                description = "Short description",
                layout = Layout()
            ), result.content
        )
    }

    @Test
    fun modelWithoutNameShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<model spid=\"8e9a7e96-df7e-4180-b93a-97bee5ce3708\">" +
                "<description>Short description</description>" +
                "<layout></layout>" +
                "</model>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MODEL, result.type)
        Assert.assertEquals(
            Model(
                spid = "8e9a7e96-df7e-4180-b93a-97bee5ce3708",
                name = null,
                description = "Short description",
                layout = Layout()
            ), result.content
        )
    }

    @Test
    fun modelWithoutDescriptionShouldBeParsed() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<model spid=\"ad27017d-720c-42e3-b886-19dfbe1062ec\">" +
                "<name>Test</name>" +
                "<layout></layout>" +
                "</model>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        val result = parser.parse(input)

        Assert.assertEquals(XmlParser.ContentType.MODEL, result.type)
        Assert.assertEquals(
            Model(
                spid = "ad27017d-720c-42e3-b886-19dfbe1062ec",
                name = "Test",
                description = null,
                layout = Layout()
            ), result.content
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun modelWithoutLayoutShouldThrowAnException() {
        val xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<model spid=\"f8aab666-837d-4486-9f6e-0c351d865157\">" +
                "<name>Test</name>" +
                "<description>Short description</description>" +
                "</model>"

        val input = xml.byteInputStream()

        val parser = XmlParser()
        parser.parse(input)

        Assert.assertTrue(false)
    }

}