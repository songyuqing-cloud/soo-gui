package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.lang.IllegalArgumentException

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class SliderTest {
    val ns: String? = null

    @Test
    fun completeSliderShouldBeParsed() {
        val xml = "<slider id=\"31fc6989-73ea-4394-a840-040995bcb35c\"" +
                " min=\"0\" max=\"1\" orientation=\"horizontal\"" +
                " step=\"0.1\" value=\"2.5\">" +
                "3.5" +
                "</slider>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "31fc6989-73ea-4394-a840-040995bcb35c",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun sliderWithoutIdAttributeShouldThrowAnException() {
        val xml = "<slider " +
                " min=\"0\" max=\"1\" orientation=\"horizontal\"" +
                " step=\"0.1\" value=\"2.5\">3.5</slider>"

        val parser = XmlHelper.buildParser(xml, ns)

        Slider.readElement(parser, ns)

        Assert.assertTrue(false)
    }

    @Test
    fun sliderWithoutValueAttributeShouldUseContentInstead() {
        val xml = "<slider id=\"3bafc26e-03ec-46fb-b8eb-3e3bb98cec48\"" +
                " min=\"0\" max=\"1\" orientation=\"horizontal\"" +
                " step=\"0.1\">" +
                "3.5" +
                "</slider>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "3bafc26e-03ec-46fb-b8eb-3e3bb98cec48",
                value = 3.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test
    fun sliderWithOnlyIdAttributeShouldShouldBeParsed() {
        val xml = "<slider id=\"da93d76c-4052-42a7-b870-c2bf5bd288db\"/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "da93d76c-4052-42a7-b870-c2bf5bd288db",
                value = 0.0,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test
    fun sliderWithoutMinAttributeShouldShouldBeParsed() {
        val xml = "<slider id=\"17ec2e1a-3132-4a9b-9178-f0d42853d86b\"" +
                " step=\"0.1\" max=\"1\" orientation=\"horizontal\"" +
                " value=\"2.5\">3.5</slider>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "17ec2e1a-3132-4a9b-9178-f0d42853d86b",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test
    fun sliderWithoutMaxAttributeShouldShouldBeParsed() {
        val xml = "<slider id=\"dd2c5b31-7a25-4bb0-bf9b-eeba1cd45f29\"" +
                " min=\"0\" step=\"0.1\" orientation=\"horizontal\"" +
                " value=\"2.5\">3.5</slider>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "dd2c5b31-7a25-4bb0-bf9b-eeba1cd45f29",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test
    fun sliderWithoutStepAttributeShouldShouldBeParsed() {
        val xml = "<slider id=\"9eb59f62-56cb-4ae6-89ca-e48aad470c63\"" +
                " min=\"0\" max=\"1\" orientation=\"horizontal\"" +
                " value=\"2.5\">3.5</slider>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "9eb59f62-56cb-4ae6-89ca-e48aad470c63",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test
    fun sliderWithoutOrientationAttributeShouldShouldBeParsed() {
        val xml = "<slider id=\"534f3c93-e0b2-458d-9988-421673869853\"" +
                " min=\"0\" max=\"1\" step=\"0.1\"" +
                " value=\"2.5\">3.5</slider>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "534f3c93-e0b2-458d-9988-421673869853",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test
    fun sliderWithoutContentShouldBeParsed() {
        val xml = "<slider id=\"52a58ad3-1eae-441d-b1fc-a0c4e041e0bd\"" +
                " min=\"0\" max=\"1\" orientation=\"horizontal\"" +
                " step=\"0.1\" value=\"2.5\"/>"

        val parser = XmlHelper.buildParser(xml, ns)

        val sliderParsed = Slider.readElement(parser, ns)

        Assert.assertEquals(
            Slider(
                id = "52a58ad3-1eae-441d-b1fc-a0c4e041e0bd",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            sliderParsed
        )
    }

    @Test
    fun everyOrientationPossibleValueShouldBeParsed() {
        val xmls = arrayOf(
            "<slider id=\"4ba5e798-bd0c-4aa0-8cbf-fa339491d95a\"" +
                    " min=\"0\" max=\"1\" orientation=\"horizontal\"" +
                    " step=\"0.1\" value=\"2.5\">" +
                    "3.5" +
                    "</slider>",
            "<slider id=\"6c9258e5-5d74-477c-ada7-c2ea01157d86\"" +
                    " min=\"0\" max=\"1\" orientation=\"vertical\"" +
                    " step=\"0.1\" value=\"2.5\">" +
                    "3.5" +
                    "</slider>"
        )

        val expected = arrayOf(
            Slider(
                id = "4ba5e798-bd0c-4aa0-8cbf-fa339491d95a",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.HORIZONTAL
            ),
            Slider(
                id = "6c9258e5-5d74-477c-ada7-c2ea01157d86",
                value = 2.5,
                step = 0.1,
                min = 0.0,
                max = 1.0,
                orientation = Slider.Orientation.VERTICAL
            )
        )

        for (i in xmls.indices) {
            val parser = XmlHelper.buildParser(xmls[i], ns)

            val orientationParsed = Slider.readElement(parser, ns)

            Assert.assertEquals(expected[i], orientationParsed)
        }
    }
}