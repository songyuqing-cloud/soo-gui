package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [29])
class TextTest {
    val ns: String? = null

    @Test
    fun completeTextShouldBeParsed() {
        val xml = "<text id=\"af6c5db3-92c5-4283-9fbe-de7d905c0f50\">Test</text>"

        val parser = XmlHelper.buildParser(xml, ns)

        val textParsed = Text.readElement(parser, ns)

        Assert.assertEquals(
            Text(
                id = "af6c5db3-92c5-4283-9fbe-de7d905c0f50",
                content = "Test"
            ),
            textParsed
        )
    }

    @Test
    fun textWithoutIdAttributeShouldBeParsed() {
        val xml = "<text>Test</text>"

        val parser = XmlHelper.buildParser(xml, ns)

        val textParsed = Text.readElement(parser, ns)

        Assert.assertEquals(
            Text(
                id = null,
                content = "Test"
            ),
            textParsed
        )
    }

    @Test
    fun textWithoutContentShouldBeParsed() {
        val xml = "<text id=\"3bbc7d52-e046-4c97-bf4e-f945c6b80e45\"></text>"

        val parser = XmlHelper.buildParser(xml, ns)

        val textParsed = Text.readElement(parser, ns)

        Assert.assertEquals(
            Text(
                id = "3bbc7d52-e046-4c97-bf4e-f945c6b80e45",
                content = ""
            ),
            textParsed
        )
    }
}