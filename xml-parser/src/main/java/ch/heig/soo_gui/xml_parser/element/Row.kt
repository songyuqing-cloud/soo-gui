package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Row(override val id: String?, var cols: List<Col> = emptyList()) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "row")
            val id = parser.getAttributeValue(ns, "id")

            val cols = listOf<Col>().toMutableList()

            while (parser.nextTag() != XmlPullParser.END_TAG) {
                // if the current element isn't a tag, then skip
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "col" -> cols.add(Col.readElement(parser, ns) as Col)
                    else -> XmlHelper.skip(parser)
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "row")
            return Row(id, cols)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (col in cols) {
            list += col.toList()
        }
        return list
    }

    override fun update(elements: List<Element>) {
        if (elements.any { e -> e !is Col }) {
            throw IllegalArgumentException("Element in Row cannot be different of Col.")
        }

        cols = elements as List<Col>
    }
}
