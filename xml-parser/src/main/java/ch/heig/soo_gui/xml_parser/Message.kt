package ch.heig.soo_gui.xml_parser

import ch.heig.soo_gui.xml_parser.element.Element

data class Message(val to: String, val content: List<Element> = emptyList())
