package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Layout(override val id: String?, val maxCols: Int, var rows: List<Row>) : Element {
    companion object : Element.ElementCompanion {
        operator fun invoke(
            id: String? = null,
            maxCols: Int? = null,
            rows: List<Row>? = null
        ): Layout {
            val maxCols = if (maxCols == null || maxCols < 1) 12 else maxCols
            return Layout(id, maxCols, rows ?: emptyList())
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "layout")
            val id = parser.getAttributeValue(ns, "id")
            val maxCols = parser.getAttributeValue(ns, "cols")?.toIntOrNull()

            val rows = listOf<Row>().toMutableList()

            while (parser.nextTag() != XmlPullParser.END_TAG) {
                // if the current element isn't a tag, then skip
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                when (parser.name) {
                    "row" -> rows.add(Row.readElement(parser, ns) as Row)
                    else -> XmlHelper.skip(parser)
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "layout")
            return Layout(id, maxCols, rows)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (row in rows) {
            list += row.toList()
        }
        return list
    }

    override fun update(elements: List<Element>) {
        if (elements.any { e -> e !is Row }) {
            throw IllegalArgumentException("Element in Layout cannot be different of Row.")
        }

        rows = elements as List<Row>
    }
}
