package ch.heig.soo_gui.xml_parser.element

import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Number(override val id: String, val step: Double, var value: Double) : Element {
    companion object : Element.ElementCompanion {
        operator fun invoke(
            id: String? = null,
            step: Double? = null,
            value: Double? = null,
        ): Number {
            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The value 'id' cannot be null or blank.")
            }
            val step = if (step == null || step <= 0.0) 1.0 else step
            return Number(id, step, value ?: 0.0)
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "number")
            val id = parser.getAttributeValue(ns, "id")
            val step = parser.getAttributeValue(ns, "step")?.toDoubleOrNull()
            var value = parser.getAttributeValue(ns, "value")?.toDoubleOrNull()


            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The attribute 'id' in <number> cannot be null or blank.")
            }

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    if (value == null) {
                        value = parser.text.toDoubleOrNull()
                    }
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "number")
            return Number(id, step, value ?: 0.0)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
