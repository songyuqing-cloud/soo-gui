package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Dropdown(
    override val id: String,
    val options: List<Option> = emptyList(),
    var selectedOption: Option?
) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "dropdown")
            val id = parser.getAttributeValue(ns, "id")
            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The attribute 'id' in <dropdown> cannot be null or blank.")
            }

            val options = listOf<Option>().toMutableList()
            while (parser.next() != XmlPullParser.END_TAG) {
                // if it isn't a tag then continue
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }
                when (parser.name) {
                    "option" -> options.add(Option.readElement(parser, ns) as Option)
                    else -> XmlHelper.skip(parser)
                }
            }

            val selectedOption = options.firstOrNull { option -> option.default }

            parser.require(XmlPullParser.END_TAG, ns, "dropdown")

            return Dropdown(id, options, selectedOption)
        }

    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (option in options) {
            list += option.toList()
        }
        return list
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
