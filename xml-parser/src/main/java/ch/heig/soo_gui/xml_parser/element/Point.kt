package ch.heig.soo_gui.xml_parser.element

import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Point(val x: String, val y: String, val z: String) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "point")

            val x = parser.getAttributeValue(ns, "x") ?: ""
            val y = parser.getAttributeValue(ns, "y") ?: ""
            val z = parser.getAttributeValue(ns, "z") ?: ""

            parser.next()

            parser.require(XmlPullParser.END_TAG, ns, "point")

            return Point(x, y, z)
        }
    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
