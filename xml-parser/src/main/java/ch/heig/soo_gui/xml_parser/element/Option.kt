package ch.heig.soo_gui.xml_parser.element

import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Option(val content: String, val value: String, val default: Boolean = false) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "option")

            val defaultString = parser.getAttributeValue(ns, "default")
            val default = defaultString != null && (defaultString == "" || defaultString == "true")
            var value = parser.getAttributeValue(ns, "value")
            var content: String? = null

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    content = parser.text
                    if (value == null) {
                        value = parser.text
                    }
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "option")

            return Option(content ?: "", value ?: "", default)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
