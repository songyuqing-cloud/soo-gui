package ch.heig.soo_gui.xml_parser.element

import android.os.Parcelable
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.lang.IllegalArgumentException

/**
 * Interface for parsing an element.
 *
 */
interface Element : Parcelable {

    val id: String?
        get() = null

    fun toList(): List<Element>

    fun update(elements: List<Element>)


    interface ElementCompanion{
        /**
         * Method implemented by the child of Element.
         * It parse an element of the same instance that the child class.
         *
         * **See Also:** [SOOUIP][https://gitlab.com/smartobject/soo-gui/docs/0001-rfc.md]
         *
         * @param parser Parser containing the different elements.
         * @return The a new instance of the concrete child class.
         */
        @Throws(XmlPullParserException::class, IOException::class, IllegalArgumentException::class)
        fun readElement(parser: XmlPullParser, ns: String?): Element
    }

    companion object {
        /**
         * Dispatch the parser to the correct element child.
         *
         * @param parser Parser containing the different elements.
         * @return The concrete child class.
         */
        @Throws(XmlPullParserException::class, IOException::class, IllegalArgumentException::class)
        fun readElement(parser: XmlPullParser, ns: String?): Element {
            return when (parser.name) {
                "button" -> Button.readElement(parser, ns)
                "col" -> Col.readElement(parser, ns)
                "dropdown" -> Dropdown.readElement(parser, ns)
                "graph" -> Graph.readElement(parser, ns)
                "input" -> Input.readElement(parser, ns)
                "label" -> Label.readElement(parser, ns)
                "layout" -> Layout.readElement(parser, ns)
                "number" -> Number.readElement(parser, ns)
                "option" -> Option.readElement(parser, ns)
                "point" -> Point.readElement(parser, ns)
                "row" -> Row.readElement(parser, ns)
                "series" -> Series.readElement(parser, ns)
                "slider" -> Slider.readElement(parser, ns)
                "text" -> Text.readElement(parser, ns)
                else -> throw IllegalArgumentException("Unknown tag ${parser.name}")
            }
        }
    }
}