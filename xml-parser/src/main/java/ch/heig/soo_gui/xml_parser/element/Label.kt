package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Label(val forId: String, val content: String = "") : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "label")
            val forId = parser.getAttributeValue(ns, "for")

            if (forId.isNullOrBlank()) {
                throw IllegalArgumentException("The attribute 'for' in <label> cannot be null or blank.")
            }

            val content = XmlHelper.readText(parser)

            parser.require(XmlPullParser.END_TAG, ns, "label")
            return Label(forId, content)
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
