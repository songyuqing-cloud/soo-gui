package ch.heig.soo_gui.xml_parser

data class Event(val from: String, val action: Action) {
    sealed class Action {
        @Suppress("ClassName")
        object CLICK_DOWN : Action()

        @Suppress("ClassName")
        object CLICK_UP : Action()

        @Suppress("ClassName")
        data class VALUE_CHANGED(val value: String) : Action()

        data class OTHER(val action: String, val value: String) : Action()
    }

    fun toXMLString(): String {
        val stringBuilder= StringBuilder()
        stringBuilder.append("<event ")
        stringBuilder.append("from=\"${this.from}\" ")

        when(this.action) {
            Action.CLICK_DOWN -> stringBuilder.append("action=\"clickDown\">")
            Action.CLICK_UP -> stringBuilder.append("action=\"clickUp\">")
            is Action.VALUE_CHANGED -> {
                stringBuilder.append("action=\"valueChanged\">")
                stringBuilder.append(this.action.value)
            }
            is Action.OTHER -> {
                stringBuilder.append("action=\"${this.action.action}\">")
                stringBuilder.append(this.action.value)
            }
        }
        stringBuilder.append("</event>")

        return stringBuilder.toString()
    }
}
