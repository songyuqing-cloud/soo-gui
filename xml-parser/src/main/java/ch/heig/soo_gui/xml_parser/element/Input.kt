package ch.heig.soo_gui.xml_parser.element

import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Input(override val id: String, var value: String) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "input")
            val id = parser.getAttributeValue(ns, "id")
            var value = parser.getAttributeValue(ns, "value")


            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The attribute 'id' in <input> cannot be null or blank.")
            }

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType == XmlPullParser.TEXT) {
                    // replace if the value isn't declared
                    if (value.isNullOrBlank()) {
                        value = parser.text
                    }
                }
            }

            parser.require(XmlPullParser.END_TAG, ns, "input")
            return Input(id, value ?: "")
        }

    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
