@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.xml_parser

import android.util.Log
import android.util.Xml
import ch.heig.soo_gui.xml_parser.XmlHelper.readText
import ch.heig.soo_gui.xml_parser.XmlHelper.skip
import ch.heig.soo_gui.xml_parser.element.Element
import ch.heig.soo_gui.xml_parser.element.Layout
import ch.heig.soo_gui.xml_parser.element.Text
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.InputStream
import java.lang.Exception
import java.lang.IllegalArgumentException

data class XmlParser(
    val ns: String? = null // We don't use namespaces but the option is here
) {
    private val TAG = "XmlParser"

    enum class ContentType {
        MODEL, MESSAGES, MOBILE_ENTITIES, UNKNOWN
    }

    data class Structure(val type: ContentType, val content: Any?)

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: InputStream): Structure {
        inputStream.use { inputStream ->
            val parser: XmlPullParser = Xml.newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            parser.setInput(inputStream, null)
            parser.nextTag()
            return readFeed(parser)
        }
    }

    private fun readFeed(parser: XmlPullParser): Structure {
        return when (parser.name) {
            "messages" -> readMessages(parser)
            "mobile-entities" -> readMobileEntities(parser)
            "model" -> readModel(parser)
            else -> Structure(ContentType.UNKNOWN, null)
        }
    }

    private fun readModel(parser: XmlPullParser): Structure {
        parser.require(XmlPullParser.START_TAG, ns, "model")

        val spid = parser.getAttributeValue(ns, "spid")
        var name: String? = null
        var description: String? = null
        var layout: Layout? = null

        while (parser.next() != XmlPullParser.END_TAG) {
            // if the current element isn't a tag, then skip
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }

            when (parser.name) {
                "name" -> name = readText(parser)
                "description" -> description = readText(parser)
                "layout" -> layout = Layout.readElement(parser, ns) as Layout
                else -> skip(parser)
            }
        }

        if (layout == null) {
            throw IllegalArgumentException("The <layout> tag in <model> doesn't exists but should exists.")
        }

        return Structure(ContentType.MODEL, content = Model(spid, name, description, layout))
    }

    private fun readMobileEntities(parser: XmlPullParser): Structure {
        val mobileEntities = mutableListOf<MobileEntity>()

        parser.require(XmlPullParser.START_TAG, ns, "mobile-entities")
        while (parser.next() != XmlPullParser.END_TAG) {
            // if the current element isn't a tag, then skip
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }

            // Starts by looking for the message tag
            if (parser.name == "mobile-entity") {
                try {
                    val mobileEntity = readMobileEntity(parser)
                    mobileEntities.add(mobileEntity)
                } catch (e:Exception) {
                    Log.e(TAG, "Cannot parse the mobile entity", e)
                }
            } else {
                skip(parser)
            }
        }
        return Structure(ContentType.MOBILE_ENTITIES, content = mobileEntities)
    }

    private fun readMobileEntity(parser: XmlPullParser): MobileEntity {
        parser.require(XmlPullParser.START_TAG, ns, "mobile-entity")

        // spid mandatory
        val spid = parser.getAttributeValue(ns, "spid")
        if (spid.isNullOrBlank()) {
            throw IllegalArgumentException("The attribute 'spid' in <mobile-entity> cannot be null or empty.")
        }

        var name: String? = null
        var description: String? = null
        while (parser.nextTag() != XmlPullParser.END_TAG) {
            // if the current element isn't a tag, then skip
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }

            when (parser.name) {
                "name" -> name = readText(parser)
                "description" -> description = readText(parser)
                else -> skip(parser)
            }
        }

        if (name.isNullOrBlank()) {
            throw IllegalArgumentException("The <name> tag in <mobile-entity> cannot be null or empty.")
        }

        return MobileEntity(spid, name, description)
    }

    private fun readMessages(parser: XmlPullParser): Structure {
        val messages = mutableListOf<Message>()

        parser.require(XmlPullParser.START_TAG, ns, "messages")
        while (parser.next() != XmlPullParser.END_TAG) {
            // if the current element isn't a tag, then skip
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }

            // Starts by looking for the message tag
            if (parser.name == "message") {
                messages.add(readMessage(parser))
            } else {
                skip(parser)
            }
        }
        return Structure(ContentType.MESSAGES, content = messages)
    }

    @Throws(XmlPullParserException::class, IOException::class, IllegalArgumentException::class)
    private fun readMessage(parser: XmlPullParser): Message {
        parser.require(XmlPullParser.START_TAG, ns, "message")
        val to = parser.getAttributeValue(ns, "to")
        val content = emptyList<Element>().toMutableList()

        if (to.isNullOrBlank()) {
            throw IllegalArgumentException("The attribute 'to' in <message> cannot be null or blank.")
        }

        // many root tag
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType == XmlPullParser.START_TAG) {
                content.add(Element.readElement(parser, ns))
            } else if (parser.eventType == XmlPullParser.TEXT) {
                // a simple string can be represented as a Text element
                content.add(Text(content = parser.text))
            }
        }

        return Message(to, content)
    }
}