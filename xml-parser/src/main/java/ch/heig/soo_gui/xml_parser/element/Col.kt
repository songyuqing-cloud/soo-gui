package ch.heig.soo_gui.xml_parser.element

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Col(
    override val id: String?,
    val span: Spanning,
    val offset: Int,
    var elements: List<Element>
) :
    Element {

    sealed class Spanning : Parcelable {
        @Parcelize
        object AUTO : Spanning()

        @Parcelize
        data class FIXED(val nbCols: Int) : Spanning()

        companion object {
            fun from(value: String?): Spanning {
                return if (
                    value == null ||
                    value == "auto" ||
                    value.toIntOrNull() == null ||
                    value.toInt() < 1
                ) AUTO else FIXED(value.toInt())
            }
        }
    }

    companion object : Element.ElementCompanion {
        operator fun invoke(
            id: String? = null,
            span: String? = null,
            offset: Int? = null,
            elements: List<Element>? = null
        ): Col {
            val span = Spanning.from(span)
            val offset = if (offset == null || offset < 0) 0 else offset
            return Col(id, span, offset, elements ?: emptyList())
        }

        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "col")
            val id = parser.getAttributeValue(ns, "id")
            val span = parser.getAttributeValue(ns, "span")
            val offset = parser.getAttributeValue(ns, "offset")?.toIntOrNull()
            val elements = listOf<Element>().toMutableList()

            while (parser.nextTag() != XmlPullParser.END_TAG) {
                // if the current element isn't a tag, then skip
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                elements.add(Element.readElement(parser, ns))
            }

            parser.require(XmlPullParser.END_TAG, ns, "col")
            return Col(id, span, offset, elements)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (elem in elements) {
            list += elem.toList()
        }
        return list
    }

    override fun update(elements: List<Element>) {
        if (elements.any { e -> e is Row || e is Col }) {
            throw IllegalArgumentException("Element in Col cannot be of Row or Col.")
        }

        this.elements = elements
    }
}
