package ch.heig.soo_gui.xml_parser.element

import ch.heig.soo_gui.xml_parser.XmlHelper
import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Text(override val id: String? = null, val content: String = "") : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "text")
            val id = parser.getAttributeValue(ns, "id")
            val content = XmlHelper.readText(parser)
            parser.require(XmlPullParser.END_TAG, ns, "text")
            return Text(id, content)
        }
    }

    override fun toList(): List<Element> {
        return arrayListOf(this)
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
