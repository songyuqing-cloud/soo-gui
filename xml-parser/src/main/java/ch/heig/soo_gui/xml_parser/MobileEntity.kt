package ch.heig.soo_gui.xml_parser

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MobileEntity(val spid: String, val name: String, val description: String? = null) : Parcelable
