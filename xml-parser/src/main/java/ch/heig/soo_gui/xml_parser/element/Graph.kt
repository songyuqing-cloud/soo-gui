package ch.heig.soo_gui.xml_parser.element

import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Graph(
    override val id: String,
    val type: Type,
    val series: List<Series> = emptyList()
) : Element {
    enum class Type {
        LINE,
        TABLE;

        companion object {
            fun from(value: String?): Type {
                return when (value) {
                    "line" -> LINE
                    else -> TABLE
                }
            }
        }
    }

    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "graph")

            val id = parser.getAttributeValue(ns, "id")
            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The attribute 'id' in <graph> cannot be null or blank.")
            }

            val typeString = parser.getAttributeValue(ns, "type")
            val type = Type.from(typeString)


            val series = listOf<Series>().toMutableList()

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                series.add(Series.readElement(parser, ns) as Series)
            }

            parser.require(XmlPullParser.END_TAG, ns, "graph")

            return Graph(id, type, series)
        }

    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (serie in series) {
            list += serie.toList()
        }
        return list
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
