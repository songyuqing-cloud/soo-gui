package ch.heig.soo_gui.xml_parser.element

import kotlinx.parcelize.Parcelize
import org.xmlpull.v1.XmlPullParser

@Parcelize
data class Series(
    override val id: String,
    val name: String,
    val points: List<Point> = emptyList()
) : Element {
    companion object : Element.ElementCompanion {
        override fun readElement(parser: XmlPullParser, ns: String?): Element {
            parser.require(XmlPullParser.START_TAG, ns, "series")

            val id = parser.getAttributeValue(ns, "id")
            if (id.isNullOrBlank()) {
                throw IllegalArgumentException("The attribute 'id' in <series> cannot be null or blank.")
            }

            val name = parser.getAttributeValue(ns, "name")

            val points = listOf<Point>().toMutableList()

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.eventType != XmlPullParser.START_TAG) {
                    continue
                }

                points.add(Point.readElement(parser, ns) as Point)
            }

            parser.require(XmlPullParser.END_TAG, ns, "series")

            return Series(id, name ?: "", points)
        }
    }

    override fun toList(): List<Element> {
        val list: MutableList<Element> = arrayListOf(this).toMutableList()
        for (point in points) {
            list += point.toList()
        }
        return list
    }

    override fun update(elements: List<Element>) {
        TODO("Not yet implemented")
    }
}
