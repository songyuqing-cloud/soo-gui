@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.pages

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ch.heig.soo_gui.KEY_MOBILE_ENTITIES_ID
import ch.heig.soo_gui.adapter.DeviceRecyclerViewAdapter
import ch.heig.soo_gui.bluetooth.*
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.viewmodel.BluetoothDevicesViewModel
import ch.heig.soo_gui.widgets.RefreshButton
import ch.heig.soo_gui.widgets.SpacingItemDecorator
import ch.heig.soo_gui.xml_parser.MobileEntity
import ch.heig.soo_gui.xml_parser.XmlParser
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.heig.soo_gui.R

class BluetoothDevicesPageFragment : Fragment() {

//    companion object {
//        fun newInstance() = BluetoothDevicesPageFragment()
//    }

    private val TAG = "BluetoothDevicesPageFragment"

    private val mBluetoothDevicesViewModel: BluetoothDevicesViewModel by viewModels({ requireParentFragment() })
    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private var mDeviceRecyclerViewAdapter: DeviceRecyclerViewAdapter? = null

    private lateinit var mRefreshButton: RefreshButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.fragment_bluetooth_devices_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        val recyclerView: RecyclerView = view.findViewById(R.id.fragment_bluetooth_devices_list)

        // create ui listing
        mBluetoothDevicesViewModel.devicesLiveData.observe(viewLifecycleOwner, { items ->
            if (mDeviceRecyclerViewAdapter == null) {
                mDeviceRecyclerViewAdapter = DeviceRecyclerViewAdapter(items) { name, address ->
                    connection(
                        name,
                        address
                    )
                }
            }

            // need to recontextualize each time the view is recreated
            with(recyclerView) {
                layoutManager = LinearLayoutManager(context)
                adapter = mDeviceRecyclerViewAdapter
            }
            recyclerView.addItemDecoration(SpacingItemDecorator())

            mDeviceRecyclerViewAdapter!!.notifyDataSetChanged()
        }) // mBluetoothDevicesViewModel.devicesLiveData

        // clear if there are already existing devices
        mBluetoothDevicesViewModel.clearDevices()

        // add events to refresh button
        mRefreshButton = view.findViewById(R.id.fragment_bluetooth_devices_refresh_btn)
        mRefreshButton.setOnClickListener {
            if (mRefreshButton.inRefreshMode) {
                cancelDeviceDiscovery()
            } else {
                startDeviceDiscovery()
            }
        }

        // ask the permissions
        askPermissions()

        // add action on Bluetooth device found, start discovery and end discovery
        val filter = IntentFilter()
        filter.addAction(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        requireActivity().registerReceiver(mReceiver, filter)

        // start discovery once all is set up
        startDeviceDiscovery()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d(TAG, "onDestroyView")
        requireActivity().unregisterReceiver(mReceiver)
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            Log.d(TAG, "received something!")
            // When discovery finds a device
            when (action) {
                BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                    Log.d(TAG, "Discovery started")
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    Log.d(TAG, "Discovery finished")
                    mRefreshButton.inRefreshMode = false
                }
                BluetoothDevice.ACTION_FOUND -> {
                    // Get the BluetoothDevice object from the Intent
                    val device =
                        intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

                    Log.d(TAG, "Found a new BT device ${device?.name} ${device?.address}")
                    mBluetoothDevicesViewModel.addDevice(device)

                }
            }
        }
    }

    private fun startDeviceDiscovery() {
        if (mBluetoothAdapter.isDiscovering) {
            cancelDeviceDiscovery()
        }
        mBluetoothAdapter.startDiscovery()
        mRefreshButton.inRefreshMode = true

        Log.d(TAG, "State : ${mBluetoothAdapter.state}") // should be 12
        Log.d(TAG, "Is discovering ? ${mBluetoothAdapter.isDiscovering}")
        Log.i(TAG, "Discovery started")
    }

    private fun cancelDeviceDiscovery() {
        mBluetoothAdapter.cancelDiscovery()
        mRefreshButton.inRefreshMode = false
        Log.i(TAG, "Discovery canceled")
    }

    private fun connection(name: String, address: String) {
        Log.i(TAG, "Trying to connect to the device $name at $address")

        cancelDeviceDiscovery()

        val device = mBluetoothDevicesViewModel.getBluetoothDevice(address)
        val connectionToast =
            Toast.makeText(
                requireContext(),
                resources.getString(R.string.try_bluetooth_connection_toast, name),
                Toast.LENGTH_LONG
            )
        connectionToast.show()

        if (device == null) {
            Log.e(TAG, "Device $name at $address not found")
            connectionToast.cancel()
            Toast.makeText(
                requireContext(),
                resources.getString(R.string.fail_bluetooth_connection_toast, name),
                Toast.LENGTH_SHORT
            )
                .show()
            return
        }

        // create socket and network handler from selected device
        mBluetoothDevicesViewModel.messageHandler = Handler(Looper.getMainLooper()) { message ->
            return@Handler when (message.what) {
                MessageType.SEND.ordinal -> {
                    Log.i(TAG, "Message received : ${String(message.obj as ByteArray)}")

                    try {
                        val parser = XmlParser()
                        val inputStream = (message.obj as ByteArray).inputStream()

                        // parse the message by its type
                        val receivedMessage = parser.parse(inputStream)
                        when (receivedMessage.type) {
                            XmlParser.ContentType.MOBILE_ENTITIES -> {

                                // the value can only be a List<MobileEntity>, so the warning is suppressed
                                @Suppress("UNCHECKED_CAST")
                                val mobileEntities = receivedMessage.content as List<MobileEntity>

                                findNavController().navigate(
                                    R.id.navigate_to_mobile_entities_page_action,
                                    bundleOf(KEY_MOBILE_ENTITIES_ID to mobileEntities.toTypedArray())
                                )
                            }
                            XmlParser.ContentType.MODEL -> throw IllegalStateException("Cannot receive a <model> message yet.")
                            XmlParser.ContentType.MESSAGES -> throw IllegalStateException("Cannot receive a <messages> message yet.")
                            XmlParser.ContentType.UNKNOWN -> throw IllegalStateException("Unknown message.")
                        }
                    } catch (e: Exception) {
                        Log.e(TAG, "Error", e)
                    }
                    true
                }
                else -> false
            }
        }
        mBluetoothDevicesViewModel.selectedBluetoothDevice.value = device

        try {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery()

            val isConnected = mBluetoothDevicesViewModel.connect()
            Log.d(TAG, "Is the device connected: $isConnected")

            // Get the mobile entities
            val getListSend = mBluetoothDevicesViewModel.send(MessageType.GET_LIST)
            Log.d(TAG, "Is GET_LIST send: $getListSend")


            connectionToast.cancel()
            Log.i(TAG, "Connection to the device $name at $address successful")
            Toast.makeText(
                requireContext(),
                resources.getString(R.string.success_bluetooth_connection_toast, name),
                Toast.LENGTH_SHORT
            )
                .show()
        } catch (e: Exception) {
            Log.e(TAG, "Cannot connect to device $name at $address", e)
            Toast.makeText(
                requireContext(),
                resources.getString(R.string.fail_bluetooth_connection_toast, name),
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    private fun askPermissions() {
        // Ask Permissions
        Log.d(TAG, "SDK : ${Build.VERSION.SDK_INT}")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    PERMISSION_REQUEST_COARSE_LOCATION
                )
            }
            if (requireActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSION_REQUEST_FINE_LOCATION
                )
            }
            if (requireActivity().checkSelfPermission(Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.BLUETOOTH),
                    PERMISSION_REQUEST_BLUETOOTH
                )
            }
            if (requireActivity().checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.BLUETOOTH_ADMIN),
                    PERMISSION_REQUEST_BLUETOOTH_ADMIN
                )
            }
        }

        // Check if Bluetooth is supported
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        if (mBluetoothAdapter == null) {
//            TODO("Device doesn't support Bluetooth")
//        }
        // Check if BT is enabled
        if (!mBluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
        }

        // Check if location is enabled. Mandatory for bluetooth scanning
        val locationManager =
            requireActivity().getSystemService(AppCompatActivity.LOCATION_SERVICE) as LocationManager
        val isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        if (!isGpsEnabled) {
            val mLocationRequest: LocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10000)
                .setFastestInterval(1000)

            val settingsBuilder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest)
            settingsBuilder.setAlwaysShow(true)

            val result: Task<LocationSettingsResponse> =
                LocationServices.getSettingsClient(requireActivity())
                    .checkLocationSettings(settingsBuilder.build())
            result.addOnCompleteListener { task ->
                try {
                    task.getResult(ApiException::class.java)
                } catch (ex: ApiException) {
                    when (ex.statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            val resolvableApiException = ex as ResolvableApiException
                            resolvableApiException
                                .startResolutionForResult(
                                    requireActivity(),
                                    REQUEST_ENABLE_LOCATION
                                )
                        } catch (e: IntentSender.SendIntentException) {
                        }

                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        }
                    }
                }
            }
        }
    }

}