@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.pages

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import ch.heig.soo_gui.KEY_MODEL_ID
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.viewmodel.BluetoothDevicesViewModel
import ch.heig.soo_gui.viewmodel.ModelViewModel
import ch.heig.soo_gui.widgets.elements.LayoutUI
import ch.heig.soo_gui.xml_parser.Message
import ch.heig.soo_gui.xml_parser.Model
import ch.heig.soo_gui.xml_parser.XmlParser
import ch.heig.soo_gui.xml_parser.element.Layout
import com.heig.soo_gui.R


class MobileEntityPageFragment : Fragment() {

//    companion object {
//        fun newInstance() = MobileEntityPageFragment()
//    }

    private val TAG = "MobileEntityPageFragment"
    private val DEBUG = false
    private val LAYOUT_TEMPLATE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<model>" +
            "  <name></name>" +
            "  <description></description>" +
            "  <layout id=\"optionalLayoutId\" max-cols=\"12\">" +
            "    <row>" +
            "      <col span=\"auto\" offset=\"0\">" +
            "      </col>" +
            "      <col span=\"1\" offset=\"1\">" +
            "        <layout id=\"innerLayoutId\">" +
            "          <row>" +
            "            <col>" +
            "            </col>" +
            "          </row>" +
            "        </layout>" +
            "      </col>" +
            "    </row>" +
            "    <row>" +
            "      <col></col>" +
            "    </row>" +
            "  </layout>" +
            "</model>"

    private val mBluetoothDevicesViewModel: BluetoothDevicesViewModel by viewModels({ requireParentFragment() })
    private lateinit var mModelViewModel: ModelViewModel
    private lateinit var mConstraintLayout: ConstraintLayout
    private lateinit var mLayoutUI: LayoutUI

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mobile_entity_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // get the ui elements
        mConstraintLayout = view.findViewById(R.id.fragment_mobile_entity_page_main_cl)
        mLayoutUI = view.findViewById(R.id.fragment_mobile_entity_page_layout_ui)

        // get the model
        val model: Model = arguments?.get(KEY_MODEL_ID) as Model? ?: Model(layout = Layout())
        mModelViewModel = ViewModelProvider(this).get(ModelViewModel::class.java)
        mModelViewModel.resetModel(model)
        mModelViewModel.modelLiveData.observe(viewLifecycleOwner, { model ->
            // update the UI
            mLayoutUI.layout = model.layout
        })

        // change the messageHandler
        mBluetoothDevicesViewModel.messageHandler = Handler(Looper.getMainLooper()) { message ->
            return@Handler when (message.what) {
                MessageType.SEND.ordinal -> {
                    Log.i(TAG, "Message received : ${String(message.obj as ByteArray)}")

                    try {
                        val parser = XmlParser()
                        val inputStream = (message.obj as ByteArray).inputStream()

                        // parse the message by its type
                        val receivedMessage = parser.parse(inputStream)
                        when (receivedMessage.type) {
                            XmlParser.ContentType.MOBILE_ENTITIES -> throw IllegalStateException("Cannot receive a <mobile-entities> message anymore.")
                            XmlParser.ContentType.MODEL -> {
                                val model = receivedMessage.content as Model
                                mModelViewModel.resetModel(model)
                            }
                            XmlParser.ContentType.MESSAGES -> {
                                val messages = receivedMessage.content as List<Message>
                                for (message in messages) {
                                    mModelViewModel.update(message.to, message.content)
                                }
                            }
                            XmlParser.ContentType.UNKNOWN -> throw IllegalStateException("Unknown message.")
                        }
                    } catch (e: Exception) {
                        Log.e(TAG, "Error", e)
                    }
                    true
                }
                else -> false
            }
        } // mBluetoothDevicesViewModel.messageHandler

        if (DEBUG) {
            val parser = XmlParser()
            val inputStream = LAYOUT_TEMPLATE.byteInputStream()
            val message = parser.parse(inputStream)
            val model = message.content as Model
            mModelViewModel.resetModel(model)
        }
    }


}