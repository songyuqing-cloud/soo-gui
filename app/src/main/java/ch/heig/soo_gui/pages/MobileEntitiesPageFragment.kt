@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.pages

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ch.heig.soo_gui.KEY_MOBILE_ENTITIES_ID
import ch.heig.soo_gui.KEY_MODEL_ID
import ch.heig.soo_gui.adapter.MobileEntityRecyclerViewAdapter
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.viewmodel.BluetoothDevicesViewModel
import ch.heig.soo_gui.viewmodel.MobileEntitiesViewModel
import ch.heig.soo_gui.widgets.RefreshButton
import ch.heig.soo_gui.widgets.SpacingItemDecorator
import ch.heig.soo_gui.xml_parser.MobileEntity
import ch.heig.soo_gui.xml_parser.Model
import ch.heig.soo_gui.xml_parser.XmlParser
import com.heig.soo_gui.R

class MobileEntitiesPageFragment : Fragment() {

//    companion object {
//        fun newInstance() = MobileEntitiesPageFragment()
//    }

    private val TAG = "MobileEntitiesPageFragment"

    private lateinit var mMobileEntitiesViewModel: MobileEntitiesViewModel
    private var mMobileEntityRecyclerViewAdapter: MobileEntityRecyclerViewAdapter? = null
    private val mBluetoothDevicesViewModel: BluetoothDevicesViewModel by viewModels({ requireParentFragment() })
    private lateinit var mRefreshButton: RefreshButton

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mobile_entities_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView: RecyclerView = view.findViewById(R.id.fragment_mobile_entities_list)
        mMobileEntitiesViewModel = ViewModelProvider(this).get(MobileEntitiesViewModel::class.java)

        // create ui listing
        mMobileEntitiesViewModel.mobileEntityLiveData.observe(viewLifecycleOwner, { items ->
            if (mMobileEntityRecyclerViewAdapter == null) {
                mMobileEntityRecyclerViewAdapter = MobileEntityRecyclerViewAdapter(items) { spid ->
                    connectMobileEntity(spid)
                }
            }

            // need to recontextualize each time the view is recreated
            with(recyclerView) {
                layoutManager = LinearLayoutManager(context)
                adapter = mMobileEntityRecyclerViewAdapter
            }

            recyclerView.addItemDecoration(SpacingItemDecorator())

            mMobileEntityRecyclerViewAdapter!!.notifyDataSetChanged()
        })

        // add events to refresh button
        mRefreshButton = view.findViewById(R.id.fragment_mobile_entities_refresh_btn)
        mRefreshButton.setOnClickListener {
            if (mRefreshButton.inRefreshMode) {
                cancelMobileEntitiesCollect()
            } else {
                startMobileEntitiesCollect()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // get the mobile entities
        val mobileEntities: Array<MobileEntity> =
            arguments?.get(KEY_MOBILE_ENTITIES_ID) as Array<MobileEntity>? ?: emptyArray()

        mMobileEntitiesViewModel.addMobileEntities(mobileEntities.toList())

        mBluetoothDevicesViewModel.messageHandler = Handler(Looper.getMainLooper()) { message ->
            return@Handler when (message.what) {
                MessageType.SEND.ordinal -> {
                    Log.i(TAG, "Message received : ${String(message.obj as ByteArray)}")

                    try {
                        val parser = XmlParser()
                        val inputStream = (message.obj as ByteArray).inputStream()

                        // parse the message by its type
                        val receivedMessage = parser.parse(inputStream)
                        when (receivedMessage.type) {
                            XmlParser.ContentType.MOBILE_ENTITIES -> {
                                val mobileEntities = receivedMessage.content as List<MobileEntity>
                                mMobileEntitiesViewModel.clearMobileEntities()
                                mMobileEntitiesViewModel.addMobileEntities(mobileEntities)
                                cancelMobileEntitiesCollect()
                            }
                            XmlParser.ContentType.MODEL -> findNavController().navigate(
                            R.id.navigate_to_mobile_entity_page_action,
                            bundleOf(KEY_MODEL_ID to receivedMessage.content as Model)
                        )
                            XmlParser.ContentType.MESSAGES -> throw IllegalStateException("Cannot receive a <messages> message yet.")
                            XmlParser.ContentType.UNKNOWN -> throw IllegalStateException("Unknown message.")
                        }
                    } catch (e: Exception) {
                        Log.e(TAG, "Error", e)
                    }
                    true
                }
                else -> false
            }
        }
    }

    private fun startMobileEntitiesCollect() {
        mRefreshButton.inRefreshMode = true
        mBluetoothDevicesViewModel.send(MessageType.GET_LIST)
    }

    private fun cancelMobileEntitiesCollect() {
        mRefreshButton.inRefreshMode = false
    }

    private fun connectMobileEntity(spid: String) {
        // Send the selection
        val isSelectSend = mBluetoothDevicesViewModel.send(MessageType.SELECT, spid)
        Log.d(TAG, "Is SELECT send: $isSelectSend")
    }
}