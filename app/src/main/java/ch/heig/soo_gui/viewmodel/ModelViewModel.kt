package ch.heig.soo_gui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ch.heig.soo_gui.xml_parser.Model
import ch.heig.soo_gui.xml_parser.element.Element
import ch.heig.soo_gui.xml_parser.element.Layout

class ModelViewModel : ViewModel() {
    private var idsMap: Map<String, Element>
    val modelLiveData = MutableLiveData<Model>()

    init {
        modelLiveData.value = Model(layout = Layout())
        idsMap = emptyMap()
    }

    /**
     * Reset the [ch.heig.soo_gui.xml_parser.Model] stored in the ViewModel.
     * In opposition to the update, it will replace all the layout.
     *
     * @param model Model to replace.
     */
    fun resetModel(model: Model) {
        modelLiveData.value = model
        idsMap = model.layout
            .toList()
            .filter { e -> !e.id.isNullOrBlank() }
            .map { e -> e.id!! to e }
            .toMap()
    }

    /**
     * Update the content inside an [ch.heig.soo_gui.xml_parser.element.Element].
     * The updated element is referred by its id.
     *
     * @param id Id of the element to update the content.
     * @param elements New elements to replace inside.
     */
    fun update(id: String, elements: List<Element>) {
        idsMap[id]?.update(elements)
    }

}