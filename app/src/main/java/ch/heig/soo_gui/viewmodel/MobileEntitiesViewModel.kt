package ch.heig.soo_gui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ch.heig.soo_gui.xml_parser.MobileEntity

class MobileEntitiesViewModel : ViewModel() {
    private val mobileEntities = ArrayList<MobileEntity>()
    val mobileEntityLiveData = MutableLiveData<ArrayList<MobileEntity>>()

    init {
        mobileEntityLiveData.value = mobileEntities
    }

    fun addMobileEntities(mobileEntities: List<MobileEntity>) {
        for (me in mobileEntities) {
            addMobileEntity(me)
        }
    }

    fun addMobileEntity(mobileEntity: MobileEntity) {
        if (!mobileEntities.contains(mobileEntity)) {
            mobileEntities.add(mobileEntity)
            mobileEntityLiveData.value = mobileEntities
        }
    }

    fun clearMobileEntities() {
        mobileEntities.clear()
        mobileEntityLiveData.value = mobileEntities
    }

}