package ch.heig.soo_gui.viewmodel

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ch.heig.soo_gui.bluetooth.BLUETOOTH_UUID
import ch.heig.soo_gui.bluetooth.Device
import ch.heig.soo_gui.network.ClientThread
import ch.heig.soo_gui.network.MessageType
import ch.heig.soo_gui.network.NetworkPlainHandler
import java.io.IOException

class BluetoothDevicesViewModel : ViewModel() {
    private val bluetoothDevices = ArrayList<BluetoothDevice>()
    private val devices = ArrayList<Device>()
    private var bluetoothSocket: BluetoothSocket? = null
    private var clientThread: ClientThread? = null
    val devicesLiveData = MutableLiveData<ArrayList<Device>>()
    val selectedBluetoothDevice = MutableLiveData<BluetoothDevice?>()
    var messageHandler: Handler? = null

    init {
        devicesLiveData.value = devices

        selectedBluetoothDevice.observeForever { device ->
            bluetoothSocket = device?.createRfcommSocketToServiceRecord(BLUETOOTH_UUID)
            if (device == null) {
                clientThread?.interrupt()
                clientThread = null
            }
        }
    }

    /**
     * Add a new device to the list.
     *
     * @param device New Bluetooth device to add.
     */
    fun addDevice(device: BluetoothDevice?) {
        // There is an assert on name not being null but it can be null.
        try {
            if (device != null &&
                !bluetoothDevices.contains(device) &&
                device.name.lowercase().contains("soo")
            ) {
                bluetoothDevices.add(device)
                devices.add(Device.from(device))
                devicesLiveData.value = devices
            }
        } catch (e: Exception) {
        }
    }

    /**
     * Clear al the devices.
     *
     */
    fun clearDevices() {
        bluetoothDevices.clear()
        devices.clear()
        devicesLiveData.value = devices
    }

    /**
     * Get the Bluetooth device by its address.
     *
     * @param address Address of the Bluetooth device.
     * @return The BluetoothDevice in question.
     */
    fun getBluetoothDevice(address: String): BluetoothDevice? {
        return bluetoothDevices.firstOrNull { it.address == address }
    }

    /**
     * Establish the connection to the bluetooth socket.
     *
     * @return True if connected or False otherwise.
     */
    @Throws(IOException::class, NullPointerException::class)
    fun connect(): Boolean {
        if (messageHandler == null) {
            throw NullPointerException("messageHandler is null.")
        }

        bluetoothSocket?.connect()

        // create and launch thread if not null
        clientThread = if (bluetoothSocket != null) {
            ClientThread(
                NetworkPlainHandler(
                    inputStream = bluetoothSocket!!.inputStream,
                    outputStream = bluetoothSocket!!.outputStream,
                    inputHandler = messageHandler!!
                )
            )
        } else {
            null
        }
        clientThread?.start()

        return bluetoothSocket != null && bluetoothSocket!!.isConnected
    }

    fun send(messageType: MessageType, spid: String? = null, payload: String? = null): Boolean {
        return if (clientThread != null) {
            clientThread!!.send(messageType, spid, payload)
            true
        } else {
            false
        }
    }
}