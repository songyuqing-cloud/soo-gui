package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.view.View
import ch.heig.soo_gui.xml_parser.element.Col
import ch.heig.soo_gui.xml_parser.element.Element
import ch.heig.soo_gui.xml_parser.element.Layout
import ch.heig.soo_gui.xml_parser.element.Row

object UIGenerator {
    fun generateUIElement(context: Context, element: Element): View? {
        return when(element) {
            is Layout -> {
                val layoutView = LayoutUI(context)
                layoutView.layout = element
                layoutView
            }
            is Row -> {
                val rowView = RowUI(context)
                rowView.row = element
                rowView
            }
            is Col -> {
                val colView = ColUI(context)
                colView.col = element
                colView
            }
            else -> null
        }
    }
}