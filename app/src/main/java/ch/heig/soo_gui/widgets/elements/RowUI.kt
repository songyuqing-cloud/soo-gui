package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TableRow
import androidx.constraintlayout.widget.Group
import ch.heig.soo_gui.xml_parser.element.Row
import com.heig.soo_gui.R

class RowUI : TableRow {

    private var _row: Row? = null

    var row: Row?
        get() = _row
        set(value) {
            _row = value
            constructRow()
            invalidate()
            requestLayout()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        inflate(context, R.layout.view_row_ui, this)
        constructRow()
    }

    private fun constructRow() {
        if (_row == null) {
            this.removeAllViews()
        } else {
            for (col in _row!!.cols) {
                val colView = UIGenerator.generateUIElement(context, col)
                if (colView != null) {

                    // add offset
                    if (col.offset > 0) {
                        val offsetGroup = Group(context)
                        offsetGroup.layoutParams = LayoutParams(
                            0, // width
                            FrameLayout.LayoutParams.MATCH_PARENT, // height
                            col.offset.toFloat() //weight
                        )
                        this.addView(offsetGroup)
                    }

                    // add col
                    this.addView(colView)
                }
            }
        }
    }
}