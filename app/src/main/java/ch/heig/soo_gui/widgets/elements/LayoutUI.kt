package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.TableLayout
import androidx.constraintlayout.widget.ConstraintLayout
import ch.heig.soo_gui.xml_parser.element.Layout
import com.heig.soo_gui.R

/**
 * Layout used in the 0001-RFC of SOOUIP.
 */
class LayoutUI : ConstraintLayout {

    private lateinit var tableLayout: TableLayout

    private var _layout: Layout? = null

    var layout: Layout?
        get() = _layout
        set(value) {
            _layout = value
            constructLayout()
            invalidate()
            requestLayout()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_layout_ui, this)
        tableLayout = findViewById(R.id.view_layout_main_tl)
        constructLayout()
    }

    private fun constructLayout() {
        if (_layout == null) {
            tableLayout.removeAllViews()
        } else {
            for (row in _layout!!.rows) {
                val rowView = UIGenerator.generateUIElement(context, row)
                if (rowView != null) {
                    tableLayout.addView(rowView)
                }
            }
        }
    }
}