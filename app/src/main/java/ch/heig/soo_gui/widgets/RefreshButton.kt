package ch.heig.soo_gui.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.heig.soo_gui.R
import kotlin.math.max

/**
 * Button with 2 state : normal mode and refreshing mode with a ProgressBar.
 */
class RefreshButton : ConstraintLayout, View.OnClickListener {

    private lateinit var constraintLayout: ConstraintLayout
    private lateinit var textView: TextView
    private lateinit var progressBar: ProgressBar

    private var _text: String? = null
    private var _textRefreshing: String? = null
    private var _inRefreshMode = false

    var text: String?
        get() = _text
        set(value) {
            _text = value
            checkMode()
            invalidate()
            requestLayout()
        }

    var textRefreshing: String?
        get() = _textRefreshing
        set(value) {
            _textRefreshing = value
            checkMode()
            invalidate()
            requestLayout()
        }

    var inRefreshMode: Boolean
        get() = _inRefreshMode
        set(value) {
            _inRefreshMode = value
            checkMode()
            invalidate()
            requestLayout()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        if (attrs != null) {
            val typedArray = context.obtainStyledAttributes(
                attrs, R.styleable.RefreshButton, defStyle, 0
            )


            _textRefreshing = typedArray.getString(R.styleable.RefreshButton_textRefreshing)
            _text = typedArray.getString(R.styleable.RefreshButton_android_text)
            _inRefreshMode = typedArray.getBoolean(R.styleable.RefreshButton_inRefreshMode, false)

            // at the end
            typedArray.recycle()
        }

        inflate(context, R.layout.view_refresh_button, this)
        constraintLayout = findViewById(R.id.view_refresh_button_cl)
        textView = findViewById(R.id.view_refresh_button_label)
        progressBar = findViewById(R.id.view_refresh_button_pb)

        // get the max text size
        textView.text = _textRefreshing
        textView.measure(0,0)
        val w1 = textView.measuredWidth
        textView.text = _text
        textView.measure(0,0)
        val w2 = textView.measuredWidth
        textView.width = max(w1, w2)

        checkMode()
    }

    private fun checkMode() {
        if(_inRefreshMode) {
            textView.text = _textRefreshing
            progressBar.visibility = VISIBLE
//            val params = textView.layoutParams as LayoutParams
//            params.startToEnd = progressBar.id

        } else {
            textView.text = _text
            progressBar.visibility = GONE
//            val params = textView.layoutParams as LayoutParams
//            params.startToStart = constraintLayout.id
        }
    }

    override fun onClick(v: View?) {
        inRefreshMode = !inRefreshMode
    }


}
