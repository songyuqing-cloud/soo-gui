package ch.heig.soo_gui.widgets.elements

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.LinearLayout
import ch.heig.soo_gui.xml_parser.element.Col
import com.heig.soo_gui.R

class ColUI : FrameLayout {

    private var _col: Col? = null
    private lateinit var linearLayout: LinearLayout

    var col: Col?
        get() = _col
        set(value) {
            _col = value
            constructCol()
            invalidate()
            requestLayout()
        }

    // The super can add attributes in this constructor overload
    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    // The super can add attributes in this constructor overload
    constructor(context: Context, attrs: AttributeSet) : super(
        context,
        attrs,
    ) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        inflate(context, R.layout.view_col_ui, this)
        linearLayout = findViewById(R.id.view_col_ui_main_ll)
        constructCol()
    }

    private fun constructCol() {
        if (_col == null) {
            linearLayout.removeAllViews()
        } else {
            // do the span attribute
            when (val span = _col!!.span) {
                is Col.Spanning.AUTO -> this.layoutParams = LayoutParams(
                    0, // width
                    LayoutParams.MATCH_PARENT // height
                )
                is Col.Spanning.FIXED -> {
                    this.layoutParams = LayoutParams(
                        0, // width
                        LayoutParams.MATCH_PARENT, // height
                        span.nbCols //weight
                    )
                }
            }


            for (element in _col!!.elements) {
                val elementView = UIGenerator.generateUIElement(context, element)
                if (elementView != null) {
                    linearLayout.addView(elementView)
                }
            }
        }
    }
}