package ch.heig.soo_gui.bluetooth

import ch.heig.soo_gui.bluetooth.BluetoothHelper.Companion.convertIntToByteArray

class Header {
    var transID = 0
    var packetID = 0
    var nbPackets = 0
    var payloadSize = 0
    var vuihandlerType = 0
    var consistency = 0
    var type = 0


    fun serialize(): ByteArray {
        val serializedHeader = ByteArray(HEADER_SIZE)

        val transIDInByte = convertIntToByteArray(transID)
        serializedHeader[8] = transIDInByte[0]
        serializedHeader[9] = transIDInByte[1]
        serializedHeader[10] = transIDInByte[2]
        serializedHeader[11] = transIDInByte[3]

        var inByte: ByteArray = convertIntToByteArray(packetID)
        serializedHeader[16] = inByte[0]
        serializedHeader[17] = inByte[1]
        serializedHeader[18] = inByte[2]
        serializedHeader[19] = inByte[3]

        inByte = convertIntToByteArray(nbPackets)
        serializedHeader[20] = inByte[0]
        serializedHeader[21] = inByte[1]
        serializedHeader[22] = inByte[2]
        serializedHeader[23] = inByte[3]

        inByte = convertIntToByteArray(payloadSize)
        serializedHeader[24] = inByte[0]
        serializedHeader[25] = inByte[1]

        serializedHeader[28] = vuihandlerType.toByte()

        serializedHeader[12] = consistency.toByte()

        serializedHeader[0] = type.toByte()


        return serializedHeader
    }
}