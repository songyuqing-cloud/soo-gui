package ch.heig.soo_gui.bluetooth

import android.bluetooth.BluetoothDevice

data class Device(
    val name: String,
    val address: String
) {
    companion object {
        fun from(bluetoothDevice: BluetoothDevice): Device {
            return Device(bluetoothDevice.name, bluetoothDevice.address)
        }
    }
}
