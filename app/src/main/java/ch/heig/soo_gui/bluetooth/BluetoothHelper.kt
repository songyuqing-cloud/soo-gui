package ch.heig.soo_gui.bluetooth

import android.util.Log

class BluetoothHelper {
    companion object {
        fun convertIntToByteArray(value: Int): ByteArray {
            val result = ByteArray(Int.SIZE_BYTES)

            val mask = 0x00FF
            var n = value

            for (i in result.indices) {
                result[i] = n.and(mask).toByte()
                n = n.shr(Byte.SIZE_BITS)
            }

            return result
        }

        fun dumpByteArray(data: ByteArray, isOutput:Boolean = false) {
            Log.i("data $isOutput", data.joinToString(" ") {
                String.format("%02X", (it.toInt() and 0xff))
            })
        }

        fun insertTransId(header: ByteArray, transID: Int) {
            val transIDInByte = convertIntToByteArray(transID)
            header[8] = transIDInByte[0]
            header[9] = transIDInByte[1]
            header[10] = transIDInByte[2]
            header[11] = transIDInByte[3]
        }

        fun insertPacketId(header: ByteArray, packet_id: Int) {
            val inByte = convertIntToByteArray(packet_id)
            header[16] = inByte[0]
            header[17] = inByte[1]
            header[18] = inByte[2]
            header[19] = inByte[3]
        }

        fun insertNrPackets(header: ByteArray, nr_packets: Int) {
            val inByte = convertIntToByteArray(nr_packets)
            header[20] = inByte[0]
            header[21] = inByte[1]
            header[22] = inByte[2]
            header[23] = inByte[3]
        }

        fun insertPayloadSize(header: ByteArray, size: Int) {
            val inByte = convertIntToByteArray(size)
            header[24] = inByte[0]
            header[25] = inByte[1]
        }
    }
}