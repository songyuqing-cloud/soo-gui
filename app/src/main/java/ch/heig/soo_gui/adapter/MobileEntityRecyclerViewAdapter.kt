@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.adapter

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import ch.heig.soo_gui.xml_parser.MobileEntity
import com.heig.soo_gui.R

data class MobileEntityRecyclerViewAdapter(
    val items: ArrayList<MobileEntity>,
    val onGetLayout: (spid: String) -> Unit
) : RecyclerView.Adapter<MobileEntityRecyclerViewAdapter.MobileEntityViewHolder>() {

    private val TAG = "MobileEntityRecyclerViewAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MobileEntityViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_mobile_entity, parent, false)
        return MobileEntityViewHolder(view)
    }

    override fun onBindViewHolder(holder: MobileEntityViewHolder, position: Int) {
        val item = items[position]
        holder.nameTextView.text = item.name
        holder.descriptionTextView.text = item.description
        holder.spid = item.spid
    }

    override fun getItemCount(): Int = items.size

    inner class MobileEntityViewHolder(view: View) :
        RecyclerView.ViewHolder(view) {
        val nameTextView: TextView = view.findViewById(R.id.item_mobile_entity_name_label)
        val descriptionTextView: TextView =
            view.findViewById(R.id.item_mobile_entity_description_label)
        var spid: String = ""
        private val seeMoreButton: Button = view.findViewById(R.id.item_mobile_entity_see_more_btn)

        init {
            seeMoreButton.setOnClickListener {
                Log.d(TAG, "Trying to get the model of ${nameTextView.text} with spid : $spid")
                onGetLayout(spid)
            }
        }
    }

}