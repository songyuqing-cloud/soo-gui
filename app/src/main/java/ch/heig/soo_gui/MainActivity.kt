@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import ch.heig.soo_gui.viewmodel.BluetoothDevicesViewModel
import com.heig.soo_gui.R


class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"

    private lateinit var mBluetoothDevicesViewModel: BluetoothDevicesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // add back button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        // add app title
        supportActionBar?.setDisplayShowTitleEnabled(true)

        // created MVVMs
        mBluetoothDevicesViewModel = ViewModelProvider(this)
            .get(BluetoothDevicesViewModel::class.java)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
            Log.i(TAG, "Pop back")
        } else {
            Log.i(TAG, "Exit Application")
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        Log.d(TAG, "Item selected")
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}