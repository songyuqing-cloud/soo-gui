@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.network

import android.os.Handler
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import kotlin.experimental.and


data class NetworkPlainHandler(
    val inputStream: InputStream,
    val outputStream: OutputStream,
    val inputHandler: Handler
) : NetworkHandler {
    private val TAG = "NetworkPlainHandler"

    override fun receive() {
        val messageSize = 1008
        val bufferReadOutputStream = ByteArrayOutputStream()
        val bufferTotal = ByteArrayOutputStream()
        var endMessage: Boolean
        var numTotalBytes = 0
        var numBytes: Int

        // get all the block of messages
        val offset = 1
        do {
            val buffer = ByteArray(messageSize)
            numBytes = inputStream.read(buffer)
            Log.d(TAG, "Number of bytes: $numBytes")
            numTotalBytes += numBytes

            bufferReadOutputStream.write(buffer, offset, numBytes - offset)
            endMessage = (buffer[0] and 0b1000_0000.toByte()) == 0b0000_0000.toByte()
        } while (numBytes != -1 && !endMessage)

        // send the message once all is received
        if (endMessage) {
            Log.d(TAG, "Number of total bytes: $numTotalBytes")
            bufferTotal.write(bufferReadOutputStream.toByteArray())

            // Send the obtained bytes to the UI Activity
            inputHandler.obtainMessage(
                MessageType.SEND.ordinal,
                numTotalBytes,
                -1,
                bufferTotal.toByteArray()
            ).sendToTarget()
        }
    }

    override fun send(type: MessageType, spid: String?, payload: String?) {
        val fullMessage = ByteArrayOutputStream()
        fullMessage.write(type.byteCode.toInt())
        if (!payload.isNullOrBlank()) {
            fullMessage.write(payload.toByteArray())
        }

        outputStream.write(fullMessage.toByteArray())
    }
}