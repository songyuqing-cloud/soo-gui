package ch.heig.soo_gui.network

enum class MessageType(val byteCode: Byte) {
    GET_LIST(0x01),
    SEND(0x02),
    SELECT(0x04),
    POST(0x08),
    UNKNOWN(0x00);

//    companion object {
//        fun from(value: Byte): MessageType {
//            return when(value.toInt()){
//                1 -> GET_LIST
//                2 -> SEND
//                4 -> SELECT
//                8 -> POST
//                else -> UNKNOWN
//            }
//        }
//    }
}