@file:Suppress("PrivatePropertyName")

package ch.heig.soo_gui.network

import android.util.Log
import java.lang.Exception
import java.util.concurrent.atomic.AtomicBoolean

data class ClientThread(val networkHandler: NetworkHandler) : Thread() {
    private val TAG = "ClientThread"
    private val running = AtomicBoolean(true)

    override fun interrupt() {
        super.interrupt()
        running.set(false)
    }

    override fun run() {
        while (running.get()) {
            try {
                networkHandler.receive()
            } catch (e: Exception) {
                Log.e(TAG, "Cannot received data anymore.", e)
                running.set(false)
            }
        }
    }

    fun send(messageType: MessageType, spid: String?, payload: String?) {
        if (running.get()) {
            try {
                networkHandler.send(messageType, spid, payload)
            } catch (e: Exception) {
                Log.e(TAG, "Cannot send data anymore.", e)
                running.set(false)
            }
        }
    }
}
