package ch.heig.soo_gui.network

interface NetworkSender {
    fun send(type: MessageType, spid: String? = null, payload: String? = null)
}