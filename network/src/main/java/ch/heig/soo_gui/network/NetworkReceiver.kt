package ch.heig.soo_gui.network

interface NetworkReceiver {
    fun receive()
}